Ad-Hoc implementation Ad-Hoc
Algoritmos_constructivos constructive-algorithms
Arboles trees
Busqueda_Binaria binary-search
Busqueda_ternaria ternary-search
Camino_mas_corto shortest-paths
Combinatoria combinatorics Combination
DFS dfs-and-similar
Divide_y_venceras divide-and-conquer
DP dp Dynamic-Programming
DSU dsu
Estructuras_con_Strings string-suffix-structures
Estructuras_de_Datos data-structures Data-Structures
Expresiones_parseo expression-parsing
FFT fft
Flujo flows
Fuerza_Bruta brute-force Brute-Force
Geometria geometry Geometry
Greedy greedy Greedy
Hashing hashing
Mascara_de_bits bitmasks
Matching graph-matchings
Matematica math Arithmetic-Algebra
Matrices matrices
Meet_in_the_midddle meet-in-the-middle
Ordenamiento sortings Sorting-Searching
Probabilidades probabilities
Simulacion schedules
Strings strings Strings
Teorema_chino_del_resto chinese-remainder-theorem
Teoria_de_Grafos graphs Graph-Theory
Teoria_de_Juegos games Game-Theory
Teoria_de_numeros number-theory Number-Theory
Two_pointers two-pointers
2-Sat 2-sat
