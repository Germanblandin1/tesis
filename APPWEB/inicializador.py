#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
#from Principal.models import Usuario
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from Principal.models import Juez
from Principal.models import Categoria
from Principal.models import Problema
from Principal.models import Pertenece
from Principal.models import Idioma
#from Principal.models import Cuenta
#from Principal.models import Resuelve


print("Comienza")
print(sys.stdout.encoding)
print("Se insertaran los jueces")
Juez.objects.all().delete()
with open("Data_Manual/Jueces.txt","r") as f:
	print("Archivo Jueces.txt Abierto exitosamente")
	for linea in f:
		Nombre,Prefijo,URL,crawler=linea.split()
		b=Juez(Nombre=Nombre,Prefijo=Prefijo,URL=URL,Crawler=crawler)
		b.save()

print("La insersecion de los jueces fue exitosa")
print("Se Insertaran los idiomas")
Idioma.objects.all().delete()
with open("Data_Manual/Idiomas.txt","r") as f:
	for linea in f:
		Nombre=linea
		b=Idioma(Nombre=Nombre)
		b.save()

print("Se insertaron los idomas correcamente")

Categoria.objects.all().delete()
print("Se insertaran las categorias")
with open("Data_Manual/Categorias.txt","r") as f:
	print("Archivo Categorias.txt Abierto exitosamente")
	for linea in f:
		Lista=linea.split()
		Nombre=Lista[0]
		Otros=" ".join(Lista[1:])
		b=Categoria(Nombre=Nombre,Otros=Otros)
		b.save()

print("Se Cargaron las categorias")
Problema.objects.all().delete()
print("Se cargaran los problemas de los jueces predeterminados")
ruta="data/"
for J in Juez.objects.all():
	print(J.pk)
	print(J.Nombre)
	print(J.Crawler)
	filename=ruta+J.Crawler+"_problem.data"
	print(filename)
	if(J.Nombre=="Coj" or J.Nombre=="Codeforces" or J.Nombre=="Uva" or J.Nombre=="Spoj"):
		with open(filename, "r",encoding="utf-8", errors="surrogateescape") as f:
			print("Archivo del juez {0} Abierto exitosamente".format(J.Nombre))
			linea=f.readline()
			while linea!="":
				Lista=linea.split()
				Nombre=Lista[0]
				dif=float(Lista[1])
				Descripcion=f.readline().split("\n")[0]
				URL=f.readline().split("\n")[0]
				tam=int(f.readline().split("\n")[0])
				OBJ=Problema(Nombre=Nombre,URL=URL,Descripcion=Descripcion,Dificultad_estadisticas=dif)
				
				try:
					print("es unico")
					OBJ.validate_unique()
				except ValidationError as e:
					print("entro en la ese")
					if(tam>0):
						Lista=f.readline().split()
					linea=f.readline()
					continue				
				OBJ.Juez_id=J;
				OBJ.save()
				print(str(OBJ))
				if(tam>0):
					Lista=f.readline().split()
					for categoria in Lista:
						for C in Categoria.objects.all():
							if(categoria in C.Otros):
								Relacion = Pertenece(Problema_id=OBJ,Categoria_id=C,Votos=20)
								Relacion.save()
								break
				linea=f.readline()

print("termino todo")
