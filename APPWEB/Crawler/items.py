# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlerItem(scrapy.Item):
	# define the fields for your item here like:
	# name = scrapy.Field()
	Nombre=scrapy.Field()
	URL=scrapy.Field()
	Descripcion=scrapy.Field()
	Categorias=scrapy.Field()
	Submits=scrapy.Field()
	Juez=scrapy.Field()
	
