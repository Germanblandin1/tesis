import scrapy
from .Dificultor import Dificultor
class Coj_problem_spider(scrapy.Spider):
	name="coj_problem"
	allowed_domains = ["coj.uci.cu"]
	
	start_urls = ['https://coj.uci.cu/24h/problems.xhtml']
	
	
	def parse(self, response):
		global f
		
		filename="data/coj_problem.data"
		f=open(filename, 'w',encoding="utf-8", errors="surrogateescape")
		problem_url="https://coj.uci.cu/tables/problems.xhtml?"
		
			
		for sel in response.xpath('//select[@name="classification"]/option'):
			value=sel.xpath('@value').extract()[0]
			if(value=="-1"):
				continue
			Categoria=sel.xpath('text()').extract()[0]
			Categoria=Categoria.replace(" ","-")
			link=problem_url+"classification="+value+"&complexity="
			for i in range(5):
				
				link2 = link+str(i+1)
				print(link2)
				request=scrapy.Request(link2, callback=self.parse_problem)
				request.meta['categoria']=Categoria
				request.meta['dif']=i+1
				yield request
			
	def parse_problem(self, response):
		tam=len(response.xpath('//table[@id="problem"]/tbody/tr[@class!="empty"]'))
		if tam==0:
			return
		for sel in response.xpath('//table[@id="problem"]/tbody/tr'):
			Nombre = sel.xpath('td[@class="headidp"]/text()').extract()[0]
			Nombre= Nombre.strip()
			Descripcion = sel.xpath('td[@style="text-transform:none"]/a/text()').extract()[0]
			Submits= sel.xpath('td[3]/a/text()').extract()[0]
			AC =  sel.xpath('td[4]/a/text()').extract()[0]
			Categoria = response.meta['categoria']
			dif = str(Dificultor().Calcular_Dificultad(AC,Submits))
			link = "https://coj.uci.cu/24h/problem.xhtml?pid="+Nombre
			f.write(Nombre+" "+dif+"\n"+Descripcion+"\n"+link+"\n1\n"+Categoria+"\n")