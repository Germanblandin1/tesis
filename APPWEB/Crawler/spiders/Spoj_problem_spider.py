import scrapy
from .Dificultor import Dificultor
class Spoj_problem_spider(scrapy.Spider):
	name="spoj_problem"
	allowed_domains=["spoj.com"]
	
	def __init__(self, usuario=None,con=10000, *args, **kwargs):
		super(Spoj_problem_spider, self).__init__(*args, **kwargs)
		self.start_urls=[]
		global g
		filename = "data/spoj_problem.data"
		g=open(filename,'w',encoding="utf-8", errors="surrogateescape")
		A = 0
		while(A<=4000):
			B = "http://www.spoj.com/problems/classical/"
			if (A!=0):
				B= B+"sort=0,start="+str(A)
			A= A+50
			self.start_urls.append(B)

			
	def parse (self,response):
		problemas = response.xpath('//div[@class="table-responsive"]/table/tbody/tr/td[@align="left"]')
		for x in problemas:
			url = x.xpath('a/@href').extract()
			Descripcion=x.xpath('a/text()').extract()[0]
			Descripcion=str(Descripcion)
			problem_url= str(url)
			print(problem_url)
			parser = ""
			tam = len(problem_url)
			y = tam-1
			while(problem_url[y]!='/'):
				if (y<tam-2):
					parser = parser + str(problem_url[y])
				y=y-1
			
			Nombre = parser[::-1];
			print(Nombre)
			Link = "http://www.spoj.com/ranks/"+Nombre
			request=scrapy.Request(Link,callback=self.parse_problem)
			request.meta['Nombre'] = Nombre
			request.meta['Descripcion'] = Descripcion
			yield request
	
	def parse_problem(self,response):
		submits = response.xpath('//table[@class="table problems"]/tbody/tr/td')
		lst=[]
		Users_AC = submits[0].xpath('text()').extract()[0]
		Submissions = submits[1].xpath('text()').extract()[0]
		AC = submits[2].xpath('text()').extract()[0]
		Link2 = "http://www.spoj.com/problems/"+response.meta["Nombre"]
		req = scrapy.Request(Link2,callback=self.parse_tags)
		req.meta['Nombre'] = response.meta["Nombre"]
		req.meta["Descripcion"]=response.meta["Descripcion"]
		req.meta['Link2'] = Link2
		req.meta['AC'] = AC
		req.meta['Submis'] = Submissions
		yield req
		
	def parse_tags(self,response):
		tags = response.xpath('//div[@id="problem-tags"]/a')
		Nombre= response.meta['Nombre']
		Descripcion= response.meta['Descripcion']
		Descripcion=Descripcion.replace(" ","-")
		URL= response.meta['Link2']
		AC= response.meta['AC']
		Total= response.meta['Submis']
		Dif=Dificultor().Calcular_Dificultad(int(AC),int(Total))
		g.write(Nombre+" "+str(Dif)+"\n")
		g.write(Descripcion+"\n")
		g.write(URL+"\n")
		#g.write("Tags:\n")
		togs = 0
		beta = []
		for cuca in tags:
			par = ""
			s = cuca.select('@href')
			s = str(s)
			tam = len(s)
			z = tam-4
			afuck=0;
			while(s[z]!='/'):
				if (s[z]==' '):
					s[z]='-'
				par = par + str(s[z])
				z=z-1
			afuck=afuck+1
			Namaeda = par[::-1]
			beta.append(Namaeda)
			togs=1
		
		if (togs==0):
			g.write("0\n")
		else:
			g.write(str(len(beta))+"\n")
			for pene in beta:
				g.write(pene+" ")
			g.write("\n")