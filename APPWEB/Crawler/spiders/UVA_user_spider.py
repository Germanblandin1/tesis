import scrapy
import json
import sys
import time
import requests
from .Dificultor import Dificultor
class UVA_user_spider(scrapy.Spider):
	name="uva_user"
	allowed_domains=["uhunt.felix-halim.net"]
	nombre_user="nadie"
	def __init__(self,usuario=None, con=10000,*args, **kwargs):
		super(UVA_user_spider, self).__init__(*args, **kwargs)
		self.nombre_user=usuario
		r = requests.get('http://uhunt.felix-halim.net/api/uname2uid/{0}'.format(usuario))
		print(str(r.status_code)+" CULOOOO")
		user_id = r.json()
		if(int(con)<10000):
			self.start_urls = ["http://uhunt.felix-halim.net/api/subs-user-last/{0}/{1}".format(user_id,str(con))]
		else:
			self.start_urls = ["http://uhunt.felix-halim.net/api/subs-user/{0}".format(user_id)]

	def parse (self,response):
		filename = "data/uva_"+self.nombre_user+".data"
		f = open(filename, "w",encoding="utf-8", errors="surrogateescape")
		jsonresponse =  json.loads(response.body.decode("utf-8","replace"))
		for sub in jsonresponse["subs"]:
			p_id=sub[1]
			status=sub[2]
			print(str(p_id)+" VS "+str(status))
			if(status==90):
				Nombre=str(p_id)
				Descripcion=Nombre
				Descripcion=Descripcion.replace(" ","-")
				URL="https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem="+Nombre
				Num_tag=0
				f.write(Nombre+" "+"0"+"\n")
				f.write(Descripcion+"\n")
				f.write(URL+"\n")
				f.write(str(Num_tag)+"\n")
		f.close()