import scrapy
import json
import sys
import time
from .Dificultor import Dificultor
class UVA_problem_spider(scrapy.Spider):
	name="uva_problem"
	allowed_domains=["uhunt.felix-halim.net"]
	start_urls = ["http://uhunt.felix-halim.net/api/p"]

	def parse (self,response):
		global f
		filename = "data/uva_problem.data"
		f = open(filename, "w",encoding="utf-8", errors="surrogateescape")
		jsonresponse =  json.loads(response.body.decode("utf-8","replace"))
		
		#json.loads(response.body_as_unicode())

		for x in jsonresponse:
			Nombre=str(x[0])
			Descripcion=str(x[2])
			Descripcion=Descripcion.replace(" ","-")
			URL="https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem="+Nombre
			Num_tag=0
			status=x[20]
			AC=x[18]
			ERR=0;
			for i in range(12,18):
				ERR=ERR+x[i]
			Total=AC+ERR
			Dif= Dificultor().Calcular_Dificultad(int(AC),int(Total))
			if status>0:
				f.write(Nombre+" "+str(Dif)+"\n")
				f.write(Descripcion+"\n")
				f.write(URL+"\n")
				f.write(str(Num_tag)+"\n")