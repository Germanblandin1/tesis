# -*- coding: utf-8 -*-
import scrapy
import json
import sys
import time
from .Dificultor import Dificultor
class Codeforces_user_spider(scrapy.Spider):
	name="codeforces_user"
	allowed_domains = ["codeforces.com"] 
	nombre_user="nadie"
	def __init__(self,usuario=None, con=10000,*args, **kwargs):
		super(Codeforces_user_spider, self).__init__(*args, **kwargs)
		self.nombre_user=usuario
		self.start_urls = ["http://codeforces.com/api/user.status?handle={0}&from=1&count={1}".format(usuario,str(con))]
	
	
	
	def parse(self, response):
		filename = "data/codeforces_"+self.nombre_user+".data"
		data = json.loads(response.body.decode("utf-8","replace"))
		f = open(filename, "w",encoding="utf-8", errors="surrogateescape")
		if data["status"]!="OK" or response.status!=200:
			f.close()
			return
		for p in data["result"]:
			Veredicto=p["verdict"]
			if (Veredicto=="OK" and len(p["problem"]["tags"])!=0):
				Nombre = str(p["problem"]["contestId"])+str(p["problem"]["index"])
				C = str(p["problem"]["index"])
				Descripcion =p["problem"]["name"]
				f.write(str(Nombre)+" ")
				Dif=str(Dificultor().Calcular_Dificultad(1,1,C))
				f.write(Dif+"\n")
				f.write(Descripcion+"\n")
				link="http://codeforces.com/problemset/problem/{0}/{1}".format(str(p["problem"]["contestId"]),C)
				f.write(link+"\n")
				tam=len(p["problem"]["tags"])
				f.write(str(tam)+"\n")
				for tag in p["problem"]["tags"]:
					f.write(str(tag).replace(" ","-")+" ")
				if(tam>0):
					f.write("\n")
				
		f.close()
		
	
	
	
	