import scrapy
from .Dificultor import Dificultor
class Coj_user_spider(scrapy.Spider):
	name="coj_user"
	allowed_domains = ["coj.uci.cu"]
	nombre_user="nadie"
	
	def __init__(self, usuario=None,con=10000, *args, **kwargs):
		super(Coj_user_spider, self).__init__(*args, **kwargs)
		self.start_urls = ['http://coj.uci.cu/user/useraccount.xhtml?username=%s' % usuario]
		f=open("data/coj_user.data", 'w')
		f.close()
		self.nombre_user=usuario
	
	def parse(self, response):
		global f
		
		filename="data/coj_"+self.nombre_user+".data"
		f=open(filename, 'w')
		problem_url="https://coj.uci.cu/24h/problem.xhtml?pid="
		
		tam=len(response.xpath('//div[@id="probsACC"]//a'))
		if tam<=0:
			return
			
		for sel in response.xpath('//div[@id="probsACC"]//a'):
			Nombre = sel.xpath('span/text()').extract()[0]
			link = problem_url+str(Nombre)
			Descripcion= sel.xpath('@title').extract()[0]
			f.write(Nombre+" "+"0"+"\n")
			f.write(Descripcion+"\n")
			f.write(link+"\n")
			f.write("0\n")
		f.close()
