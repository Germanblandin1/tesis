import scrapy
class Spoj_user_spider(scrapy.Spider):
	name="spoj_user"
	allowed_domains = ["spoj.com"]
	nombre_user="nadie"
	def __init__(self,usuario=None,con=10000,*args,**kwargs):
		super(Spoj_user_spider,self).__init__(*args,**kwargs)
		self.nombre_user=usuario
		self.start_urls = ['http://www.spoj.com/users/{0}'.format(usuario)]

	
	def parse(self,response):
		filename = "data/spoj_"+self.nombre_user+".data"
		problem_url = 'http://www.spoj.com/problems/'
		f = open(filename, "w",encoding="utf-8", errors="surrogateescape")
		broma = response.xpath('//table[@class="table table-condensed"]/tr/td')
		for sel in broma:
			nom = sel.xpath('a/text()').extract()
			if (len(nom)>0):
				Nombre = nom[0]
				URL = problem_url+nom[0]
				Descripcion = nom[0]
				f.write(Nombre + " 0\n")
				f.write(Descripcion+"\n")
				f.write(URL+"\n0\n")
		f.close()