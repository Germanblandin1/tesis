# -*- coding: utf-8 -*-
import scrapy
import json
import sys
import time
from .Dificultor import Dificultor
class Codeforces_problem_spider(scrapy.Spider):
	name="codeforces_problem"
	allowed_domains = ["codeforces.com"]
	contador=0
	start_urls=[
		"http://codeforces.com/api/problemset.problems"
	]
		
		
	
	def parse(self, response):
		filename = "data/codeforces_problem.data"
		data = json.loads(response.body.decode("utf-8","replace"))
		if data["status"]!="OK":
			return
		modo="a"
		if self.contador==0:
			modo="w"
		self.contador+=1
		f = open(filename, modo,encoding="utf-8", errors="surrogateescape")
		i=0
		for p in data["result"]["problems"]:
			Nombre = str(p["contestId"])+str(p["index"])
			C = str(p["index"])
			Descripcion =p["name"]
			AC=int(data["result"]["problemStatistics"][i]["solvedCount"])
			Dif=str(Dificultor().Calcular_Dificultad(AC,1,C))
			f.write(str(Nombre)+" ")
			f.write(Dif+"\n")
			f.write(str(Descripcion)+"\n")
			link="http://codeforces.com/problemset/problem/{0}/{1}".format(str(p["contestId"]),C)
			f.write(link+"\n")
			tam=len(p["tags"])
			f.write(str(tam)+"\n")
			for tag in p["tags"]:
				f.write(str(tag).replace(" ","-")+" ")
			if(tam>0):
				f.write("\n")
			i+=1
		f.close()
		
	
	
	
	