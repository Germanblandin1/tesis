

class Dificultor:
	
	def codeforces(self,Letra,AC):
		dificultad=10
		if(Letra=="A"):
			if AC>3000:
				dificultad=1
			else:
				dificultad=2
		elif (Letra=="B"):
			if AC>2000:
				dificultad=2
			else:
				dificultad=3
		elif (Letra=="C"):
			if AC>1000:
				dificultad=3
			else:
				dificultad=4
		elif (Letra=="D"):
			if AC>400:
				dificultad=4
			else:
				dificultad=5
		elif (Letra=="E"):
			if AC>100:
				dificultad=6
			else:
				dificultad=7
		elif (Letra=="F"):
			if AC>50:
				dificultad=8
			else:
				dificultad=9
		elif (Letra=="G"):
			if AC>50:
				dificultad=9
			else:
				dificultad=10
		elif (Letra=="H"):
			dificultad=10
		else:
			dificultad=0
		return dificultad
	
	def Mayor10000(self,AC,submits):
		ratio=AC/submits
		dificultad=0
		if (ratio>=0.35):
			dificultad=1
		elif (ratio>=0.22):
			dificultad=2
		elif (ratio>=0.18):
			dificultad=3
		elif (ratio>=0.16):
			dificultad=4
		elif (ratio>=0.13):
			dificultad=5
		elif (ratio>=0.10):
			dificultad=6
		elif (ratio>=0.07):
			dificultad=7
		elif (ratio>=0.05):
			dificultad=8
		elif (ratio>=0.02):
			dificultad=9
		elif (ratio>=0):
			dificultad=10
		return dificultad
	
	def Mayor1000(self,AC,submits):
		ratio=AC/submits
		dificultad=0
		if (ratio>=0.55):
			dificultad=1
		elif (ratio>=0.42):
			dificultad=2
		elif (ratio>=0.38):
			dificultad=3
		elif (ratio>=0.28):
			dificultad=4
		elif (ratio>=0.20):
			dificultad=5
		elif (ratio>=0.15):
			dificultad=6
		elif (ratio>=0.1):
			dificultad=7
		elif (ratio>=0.08):
			dificultad=8
		elif (ratio>=0.05):
			dificultad=9
		elif (ratio>=0):
			dificultad=10
		return dificultad
			
	def Mayor100(self,AC,submits):
		ratio=AC/submits
		dificultad=0
		if (ratio>=0.75):
			dificultad=1
		elif (ratio>=0.52):
			dificultad=2
		elif (ratio>=0.45):
			dificultad=3
		elif (ratio>=0.38):
			dificultad=4
		elif (ratio>=0.28):
			dificultad=5
		elif (ratio>=0.20):
			dificultad=6
		elif (ratio>=0.15):
			dificultad=7
		elif (ratio>=0.10):
			dificultad=8
		elif (ratio>=0.07):
			dificultad=9
		elif (ratio>=0):
			dificultad=10
		return dificultad
	
	def Mayor10(self,AC,submits):
		ratio=AC/submits
		dificultad=0
		if (ratio>=0.95):
			dificultad=1
		elif (ratio>=0.80):
			dificultad=2
		elif (ratio>=0.70):
			dificultad=3
		elif (ratio>=0.60):
			dificultad=4
		elif (ratio>=0.50):
			dificultad=5
		elif (ratio>=0.40):
			dificultad=6
		elif (ratio>=0.30):
			dificultad=7
		elif (ratio>=0.20):
			dificultad=8
		elif (ratio>=0.10):
			dificultad=9
		elif (ratio>=0):
			dificultad=10
		return dificultad
	
	def Calcular_Dificultad(self,AC,submits,letra="Z"):
		AC=float(AC)
		submits=float(submits)
		if(submits==0):
			return 10
		if letra!="Z":
			return self.codeforces(letra,AC)
		if submits>=10000:
			return self.Mayor10000(AC,submits)
		if submits>=1000:
			return self.Mayor1000(AC,submits)
		if submits>=100:
			return self.Mayor100(AC,submits)
		return self.Mayor10(AC,submits)