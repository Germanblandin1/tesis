from django.core.exceptions import ValidationError, NON_FIELD_ERRORS, ObjectDoesNotExist
from django.db.models.query import EmptyQuerySet
from .models import *
from scipy.spatial.distance import *
from math import sqrt,floor,ceil
from decimal import *
from .apyori import apriori
import random
from time import time
from Principal.models import *
from decimal import *
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS, ObjectDoesNotExist
def sacar_transacciones():
	Transacciones=[]
	n=0
	for U in Usuario.objects.all():
		tran=[]
		for R in Resuelve.objects.filter(Usuario_id=U,Resuelto=True):
			tran.append(R.Problema_id.pk)
		Transacciones.append(tran)
		n+=1
	print("Proceso de Transacciones termino ahora escribira")
	f = open("data/Transacciones", "w")
	f.write(str(n)+"\n")
	for T in Transacciones:
		for L in T:
			f.write(str(L)+" ")
		f.write("\n")
	f.close()
	print("Ya escribio transacciones")

def sacar_matriz():
	Matriz=[]
	i=0
	j=1
	cod_inicio=0
	cod_fin=0
	Problemas_map={}
	Problemas=[]
	for U in Usuario.objects.all():
		if(i==0):
			cod_inicio=U.id
		lista=[U.id]
		lista.extend([0 for x in range(j-1)])
		Problemas_aux=Resuelve.objects.filter(Usuario_id=U,Resuelto=True)
		for P in Problemas_aux:
			Codigo=P.Problema_id
			if Codigo in Problemas_map:
				lista[Problemas_map[Codigo]]=1
			else:
				Problemas_map[Codigo]=j
				Problemas.append(Codigo)
				lista.append(1)
				j=j+1
		i=i+1
		Matriz.append(lista)
		cod_fin=U.id
	n=i
	m=j
	i=0
	for L in Matriz:
		if len(L)!=j:
			falta=j-len(L)
			Matriz[i].extend([0 for x in range(falta)])
		i=i+1
	print("Proceso de Matriz termino ahora escribira")
	f = open("data/Matriz", "w")
	f.write(str(cod_inicio)+" "+str(cod_fin)+"\n")
	f.write(str(n)+" "+str(m)+"\n")
	for fila in Matriz:
		for C in fila:
			f.write(str(C)+" ")
		f.write("\n")
	for P in Problemas:
		f.write(str(P.id)+" ")
	f.write("\n")
	f.close()
	print("Ya escribio matriz")

sacar_transacciones()
sacar_matriz()