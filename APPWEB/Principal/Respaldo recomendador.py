from django.core.exceptions import ValidationError, NON_FIELD_ERRORS, ObjectDoesNotExist
from django.db.models.query import EmptyQuerySet
from .models import *
import pandas as pd
import numpy as np
from scipy.spatial.distance import *
from math import sqrt,floor,ceil
from decimal import *
from .apyori import apriori
import random
from time import time

class Recomendador:
	"""Clase que realiza recomendaciones con distintos metodos"""




	def APRIORI(self,User,min_support,min_confidence,min_lift,max_length,K):
		Nivel=ceil(User.Nivel)
		if(Nivel==0):
			Nivel+=1
		Resueltos_u=Resuelve.objects.filter(Usuario_id=User,Resuelto=True)
		Resuelto_mapa=set()
		for R in Resueltos_u:
			Resuelto_mapa.add(R.Problema_id.pk)
		Tiempo_ini=time()
		Transacciones=[]
		for U in Usuario.objects.all():
			Nivel_u=int(floor((U.Nivel)))
			if(Nivel_u==0):
				Nivel_u=1
			tran=[]
			for R in Resuelve.objects.filter(Usuario_id=U,Resuelto=True):
				tran.append(R.Problema_id.pk)
			Transacciones.append(tran)
		Tiempo_fin=time()
		print("Tarda: "+str(Tiempo_fin-Tiempo_ini))
		print("min supol 3"+str(min_support))
		Recomendaciones_i=self.r_apriori(min_support,min_confidence,min_lift,max_length,K,Nivel,Resuelto_mapa,Transacciones)
		Recomendaciones=[]
		for R in Recomendaciones_i:
			P=Problema.objects.get(pk=R)
			Recomendaciones.append(P)
		return Recomendaciones


	def r_apriori(self,min_suppor,min_confidenc,min_lif,max_lengt,K,Nivel,Resuelto_mapa,Transacciones):
		print("min supol 4"+str(min_suppor))
		print("Transacciones "+str(len(Transacciones)))
		resultas = list(apriori(Transacciones,min_support=min_suppor,min_confidence=min_confidenc,min_lift=min_lif,max_length=max_lengt))
		print(len(resultas))
		results=[]
		for R in resultas:
			print(R[0])
			results.append(list(R[0]))
		results.reverse()
		iteraciones=0
		max_iteraciones=1000
		Recomendaciones=[]
		llevo=0
		for T in results:
			for P in T:
				if((P not in Resuelto_mapa)):
					Recomendaciones.append(P)
					llevo+=1
					if llevo>=K:
						return Recomendaciones
			iteraciones+=1
			if(iteraciones>=max_iteraciones):
				return Recomendaciones
		return Recomendaciones



	#tipo 0 es basado en contenido
	#tipo 1 es colaborativo

	def distancia(self,opc,v1,v2):
		if(opc=="cos"):
			return self.dista_coseno(v1,v2)
		elif(opc=="euc"):
			return self.dista_euclid(v1,v2)
		elif(opc=="ham"):
			return self.dista_hamming(v1,v2)
		elif(opc=="bra"):
			return self.dista_braycurtis(v1,v2)
		else:
			return self.dista_coseno(v1,v2)


	def dista_coseno(self,v1,v2):
		return 1-cosine(v1,v2)

	def dista_euclid(self,v1,v2):
		return euclidean(v1,v2)

	def dista_hamming(self,v1,v2):
		return hamming(v1,v2)

	def dista_braycurtis(self,v1,v2):
		return braycurtis(v1,v2)

	def contenido(self,User,Problemas,Nivel,K):
		#User El usuario al que se le realiza la recomendacion
		#Problemas Candidatos a recomendaciones Ya previamente filtrados
		#Nivel indica el nivel aproximado que se pide de las recomendaciones
		#K cantidad de recomendaciones solicitadas
		#una vez obtenido los problemas procedemos a obtener los datos para la recomendacion
		print("hay "+str(len(Problemas))+" sin resolver")
		if Problemas!=[]:
			#A promedio de la pagina de cada problema
			#B promedio ponderado de los niveles de uusarios de nuestra pagina que lo han resuelto
			#esto se calcula recorriendo el voto de cada usuario y viendo el nivel que tenia al resolver ese problema.
			#C promedio de los votos de los usuarios
			Salida=[]
			for P in Problemas:
				A=P.Dificultad_estadisticas
				B=P.Dificultad_resuelto
				C=P.Dificultad_usuarios
				can=1
				if B!=0:
					can=can+1
				if C!=0:
					can=can+1
				AVG=(A+B+C)/can
				if(AVG==0):
					continue
				desviacion = (A*A + B*B + C*C)/can - (AVG*AVG)
				if(Nivel+1>=round(AVG) and Nivel<=round(AVG)):
					Salida.append( (sqrt(float(desviacion))/float(AVG),P) ) #desviacion standard / promedio es la similaridad relativa
			Salida= sorted(Salida, key=lambda Salida: Salida[0])
			Recomendaciones1=Salida[:K]
			Recomendaciones=[]
			for x in Recomendaciones1:
				Recomendaciones.append(x[1])
			return Recomendaciones
		else:
			return []

	def colaborativo(self,User,Categ,K,Pp,Opc):
		#User usuario al cual se le hace la recomendacion
		#Nivel nivel aproximado en que se quieren las recomendaciones
		#K numero de vecinos para el K-vecinos
		#Pp cantidad de problemas que se quieren
		#creamos la matriz de boleanos Usuarios-problemas donde una posicion i,j indica que el usuario i resolvio el problema j
		Matriz=[]
		indice_user=-1   #indice del usuario en la matriz
		i=0
		j=1
		Tiempo_ini=time()
		Problemas_map={}
		Problemas=[]
		for U in Usuario.objects.all():
			lista=[U.id]
			lista.extend([0 for x in range(j-1)])
			Problemas_aux=Resuelve.objects.filter(Usuario_id=U,Resuelto=True)
			for P in Problemas_aux:
				Codigo=P.Problema_id
				if Codigo in Problemas_map:
					lista[Problemas_map[Codigo]]=1
				else:
					Problemas_map[Codigo]=j
					Problemas.append(Codigo)
					lista.append(1)
					j=j+1
			if U.id==User.id:
				indice_user=i
			#print("termino el usuario")
			i=i+1
			Matriz.append(lista)
		i=0
		for L in Matriz:
			if len(L)!=j:
				falta=j-len(L)
				Matriz[i].extend([0 for x in range(falta)])
			i=i+1
		Tiempo_fin=time()
		print("Tarda: "+str(Tiempo_fin-Tiempo_ini))
		#Una vez obtenida la matriz de 0 y 1 comparamos los usuarios
		id_user=User.id
		Y=[]
		i=0
		for Res in Matriz:
			if Res[0]==id_user:   #si es el mismo usuario ignoramos
				pass
			else:
				print(len(Matriz[indice_user]))
				print(len(Res))
				Y.append( (self.distancia(Opc,Matriz[indice_user],Res),i) ) #calculamos la similaridad entre el usuario actual y los demas
			i=i+1
		print("Termino de comparar")
		similares= sorted(Y, key=lambda Y: Y[0]) #entre mayor es el indice mas se parecen, asi que hay que invertirlas
		similares= similares[::-1] #aqui invertimos
		similares= similares[:K] #aqui tomamos los K mas cercanos
		print("Ordeno y hay usuarios similares"+str(len(similares)))
		#una vez tenemos los usuarios ordenados por similaridad procedemos a buscar los problemas a recomendar
		Recomendaciones=[]
		for U in similares:
			actual=U[1]
			j=0
			for P in Matriz[actual]:
				if(j!=0 and P==1 and Matriz[indice_user][j]==0):
					Recomendaciones.append(Problemas[j-1])  #guardamos todoslos problemas que los K-vecinos hayan resuelto y el usuario no
				j=j+1
		print("Saco los problemas")
		#Lor ordenamos de manera random y retornamos los P solicitados independientemente del nivel
		if len(Recomendaciones)==0:
			return []
		#seria aconsejable randomizar esta lista
		return Recomendaciones[:Pp]






