from django.core.exceptions import ValidationError, NON_FIELD_ERRORS, ObjectDoesNotExist
from django.db.models.query import EmptyQuerySet
from .models import *
from scipy.spatial.distance import *
from math import sqrt,floor,ceil
from decimal import *
from .apyori import apriori
import random
from time import time

class Recomendador:
	"""Clase que realiza recomendaciones con distintos metodos"""


	def Cargar_Transacciones(self):
		Transacciones=[]
		with open("data/Transacciones", "r") as f:
			n=f.readline().split()[0]
			n=int(n)
			for i in range(n):
				lista=[int(x) for x in f.readline().split()]
				Transacciones.append(lista)
		return Transacciones

	def APRIORI(self,User,min_support,min_confidence,min_lift,max_length,K):
		Nivel=ceil(User.Nivel)
		if(Nivel==0):
			Nivel+=1
		Resueltos_u=Resuelve.objects.filter(Usuario_id=User,Resuelto=True)
		Resuelto_mapa=set()
		for R in Resueltos_u:
			Resuelto_mapa.add(R.Problema_id.pk)
		Tiempo_ini=time()
		Transacciones=self.Cargar_Transacciones()
		Tiempo_fin=time()
		print("Tarda: "+str(Tiempo_fin-Tiempo_ini))
		print("min supol 3"+str(min_support))
		Recomendaciones_i=self.r_apriori(min_support,min_confidence,min_lift,max_length,K,Nivel,Resuelto_mapa,Transacciones)
		Recomendaciones=[]
		for R in Recomendaciones_i:
			P=Problema.objects.get(pk=R)
			Recomendaciones.append(P)
		return Recomendaciones


	def r_apriori(self,min_suppor,min_confidenc,min_lif,max_lengt,K,Nivel,Resuelto_mapa,Transacciones):
		print("min supol 4"+str(min_suppor))
		print("Transacciones "+str(len(Transacciones)))
		resultas = list(apriori(Transacciones,min_support=min_suppor,min_confidence=min_confidenc))
		print(len(resultas))
		results=[]
		for R in resultas:
			results.append(list(R[0]))
		results.reverse()
		iteraciones=0
		max_iteraciones=1000
		Recomendaciones=[]
		Problemas_set=set()
		llevo=0
		for T in results:
			for P in T:
				if((P not in Resuelto_mapa) and (P not in Problemas_set)):
					Problemas_set.add(P)
					Recomendaciones.append(P)
					llevo+=1
					if llevo>=K:
						return Recomendaciones
			iteraciones+=1
			if(iteraciones>=max_iteraciones):
				return Recomendaciones
		return Recomendaciones



	#tipo 0 es basado en contenido
	#tipo 1 es colaborativo

	def distancia(self,opc,v1,v2):
		if(opc=="cos"):
			return self.dista_coseno(v1,v2)
		elif(opc=="euc"):
			return self.dista_euclid(v1,v2)
		elif(opc=="ham"):
			return self.dista_hamming(v1,v2)
		elif(opc=="bra"):
			return self.dista_braycurtis(v1,v2)
		else:
			return self.dista_coseno(v1,v2)


	def dista_coseno(self,v1,v2):
		return cosine(v1,v2)

	def dista_euclid(self,v1,v2):
		return euclidean(v1,v2)

	def dista_hamming(self,v1,v2):
		return hamming(v1,v2)

	def dista_braycurtis(self,v1,v2):
		return braycurtis(v1,v2)

	def contenido(self,User,Problemas,Nivel,K):
		#User El usuario al que se le realiza la recomendacion
		#Problemas Candidatos a recomendaciones Ya previamente filtrados
		#Nivel indica el nivel aproximado que se pide de las recomendaciones
		#K cantidad de recomendaciones solicitadas
		#una vez obtenido los problemas procedemos a obtener los datos para la recomendacion
		print("hay "+str(len(Problemas))+" sin resolver")
		if Problemas!=[]:
			#A promedio de la pagina de cada problema
			#B promedio ponderado de los niveles de uusarios de nuestra pagina que lo han resuelto
			#esto se calcula recorriendo el voto de cada usuario y viendo el nivel que tenia al resolver ese problema.
			#C promedio de los votos de los usuarios
			Salida=[]
			for P in Problemas:
				A=P.Dificultad_estadisticas
				B=P.Dificultad_resuelto
				C=P.Dificultad_usuarios
				can=1
				if B!=0:
					can=can+1
				if C!=0:
					can=can+1
				AVG=(A+B+C)/can
				if(AVG==0):
					continue
				desviacion = (A*A + B*B + C*C)/can - (AVG*AVG)
				if(Nivel+1>=round(AVG) and Nivel<=round(AVG)):
					Salida.append( (sqrt(float(desviacion))/float(AVG),P) ) #desviacion standard / promedio es la similaridad relativa
			Salida= sorted(Salida, key=lambda Salida: Salida[0])
			Recomendaciones1=Salida[:K]
			Recomendaciones=[]
			for x in Recomendaciones1:
				Recomendaciones.append(x[1])
			return Recomendaciones
		else:
			return []
	
	def Cargar_Matriz(self,User):
		with open("data/Matriz", "r") as f:
			Codigo_ini,Codigo_fin=f.readline().split()
			Codigo_ini=int(Codigo_ini)
			Codigo_fin=int(Codigo_fin)
			n,m=f.readline().split()
			n=int(n)
			m=int(m)
			Matriz=[]
			for i in range(n):
				lista=[int(x) for x in f.readline().split()]
				Matriz.append(lista)
			Problemas_map={}
			Problemas=[]
			j=0
			for x in f.readline().split():
				Problemas_map[int(x)]=j
				Problemas.append(int(x))
				j+=1
		if(User.id>Codigo_fin):
			lista=[User.id]
			lista.extend([0 for x in range(m-1)])
			for R in Resuelve.objects.filter(Usuario_id=User,Resuelto=True):
				if(R.Problema_id.pk in Problemas_map):
					lista[Problemas_map[R.Problema_id.pk]]=1
			Matriz.append(lista)
			indice_user=n
			print("MIRAAAAAAAAAAAAAAAA")
		else:
			indice_user=User.id-Codigo_ini
		
		return Matriz,Problemas,indice_user
			
	
	
	def colaborativo(self,User,Categ,K,Pp,Opc):
		Tiempo_ini=time()
		Matriz,Problemas,indice_user=self.Cargar_Matriz(User)
		Tiempo_fin=time()
		print("Tarda: "+str(Tiempo_fin-Tiempo_ini))
		#Una vez obtenida la matriz de 0 y 1 comparamos los usuarios
		id_user=User.id
		Y=[]
		i=0
		for Res in Matriz:
			if Res[0]==id_user:   #si es el mismo usuario ignoramos
				pass
			else:
				Y.append( (self.distancia(Opc,Matriz[indice_user][1:],Res[1:]),i) ) #calculamos la similaridad entre el usuario actual y los demas
			i=i+1
		print("Termino de comparar")
		similares= sorted(Y, key=lambda Y: Y[0]) #entre mayor es el indice mas se parecen, asi que hay que invertirlas
		#similares.reverse() #aqui invertimos
		similares= similares[:K] #aqui tomamos los K mas cercanos
		print("Ordeno y hay usuarios similares"+str(len(similares)))
		#una vez tenemos los usuarios ordenados por similaridad procedemos a buscar los problemas a recomendar
		Recomendaciones=[]
		Usuarios=[]
		Valores=[]
		can=0
		Problemas_set=set()
		for U in similares:
			actual=U[1]
			Usuarios.append(Usuario.objects.get(pk=Matriz[actual][0]))
			Valores.append(round(U[0]*10000000)/10000000)
			j=0
			if(can<Pp):
				for P in Matriz[actual]:
					if(j!=0 and P==1 and Matriz[indice_user][j]==0 and (j in Problemas_set)==False):
						Pro=Problema.objects.get(pk=Problemas[j-1])
						Recomendaciones.append(Pro)  #guardamos todoslos problemas que los K-vecinos hayan resuelto y el usuario no
						Problemas_set.add(j)
						can+=1
						if(can==Pp):
							break
					j=j+1
			
		print("Saco los problemas")
		#Lor ordenamos de manera random y retornamos los P solicitados independientemente del nivel
		if len(Recomendaciones)==0:
			return [],[],[]
		#seria aconsejable randomizar esta lista
		
		return Recomendaciones[:Pp],Usuarios,Valores






