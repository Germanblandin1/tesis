# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-04-17 12:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Principal', '0012_auto_20170417_0757'),
    ]

    operations = [
        migrations.AddField(
            model_name='tutorial',
            name='Imagen',
            field=models.ImageField(null=True, upload_to='Tutoriales'),
        ),
    ]
