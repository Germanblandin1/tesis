# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-02-22 21:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Principal', '0007_auto_20170222_1738'),
    ]

    operations = [
        migrations.AddField(
            model_name='problema',
            name='Votos_asig',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='problema',
            name='Votos_nivel',
            field=models.IntegerField(default=0),
        ),
    ]
