from django.conf.urls import url

from . import views
app_name = 'Principal'


urlpatterns = [
    url(r'^$', views.index, name='index'),
	url(r'^index.html$', views.manage_static_view, name='index'),
	url(r'^problemas.html$', views.view_problemas, name='problemas'),
	url(r'^portfolio.html$', views.view_tutoriales, name='portfolio'),
	url(r'^profile.html$', views.view_profile, name='profile'),
	url(r'^Recomendaciones.html$', views.view_recomendaciones, name='Recomendaciones'),
	url(r'^Contenido$', views.view_recomendaciones, name='Contenido'),
	url(r'^Colaborativo$', views.view_recomendaciones, name='Colaborativo'),
	url(r'^Apriori$', views.view_recomendaciones, name='Apriori'),
	url(r'^update$', views.update_problems, name='update'),
	url(r'^update_habilidad$', views.update_habilidad, name='update_habilidad'),
	url(r'^clasificar/(?P<problem_id>[0-9]+)$', views.view_clasifica, name='clasificar'),
	url(r'^Buscar$', views.view_problemas, name='Buscar'),
	url(r'^acounts$', views.update_acounts, name='update'),
	url(r'^signup.html$', views.get_login, name='signup'),
	url(r'^logout$', views.log_out, name='logout'),
	url(r'^GoogleLogin$', views.get_login, name='GoogleLogin'),
	url(r'^FacebookLogin$', views.get_login, name='FacebookLogin'),
	url(r'^GoogleRegister$', views.get_user, name='GoogleRegister'),
	url(r'^register.html$', views.get_user, name='register'),
	url(r'^Clasificacion.html$', views.view_Clasificacion, name='Clasificacion'),
	url(r'^(?P<problem_id>[0-9]+)/Clasificacion.html$', views.view_clasifica, name='timeline'),
	url(r'^(?P<pagina_id>[0-9]+)/Pagina$', views.view_problemas2, name='Paginacion'),
	url(r'^Next$', views.view_problemas3, name='Next'),
	url(r'^Prev$', views.view_problemas3, name='Prev'),
	url(r'^Prim$', views.view_problemas3, name='Prim'),
	url(r'^Ult$', views.view_problemas3, name='Ult'),
	url(r'^recomendar$', views.view_recomendar, name='recomendar'),
	url(r'^Negro$', views.cambia_color, name='Negro'),
	url(r'^Blanco$', views.cambia_color, name='Blanco'),
]
