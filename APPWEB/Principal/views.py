from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.http import HttpResponseRedirect
from django.http import Http404
from django.template import loader
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS, ObjectDoesNotExist
from scipy.spatial.distance import *
from math import sqrt
from decimal import *
from .models import Usuario
from .models import Juez
from .models import Categoria
from .models import Pertenece
from .models import Cuenta
from .models import Problema
from .models import Resuelve
from .models import Habilidad
from .models import Tutorial
from .models import Pais
from .forms import NameForm
from .forms import LoginForm
from .forms import CuentaForm
from .forms import ClasificacionForm
from .forms import ProblemForm
from .forms import ContenidoForm
from .forms import ColaborativoForm
from .forms import AprioriForm
from .forms import GoogleForm
from .Recomendador import Recomendador
import subprocess
from time import time
import operator
import random

def cambia_color(request):
	print("Si entro")
	if "Negro" in request.path:
		print("Fondo de Hombres")
		request.session['Color'] = 'Negro'
	else:
		print("Fondo de maricos")
		request.session['Color'] = "Blanco"
	return HttpResponseRedirect(request.session["PREURL"])

def log_out(request):
	try:
		del request.session['User_id']
	except KeyError:
		pass
	return HttpResponseRedirect('index.html')

def peticion_post(request):
	if ("Contenido" in request.path) or("Colaborativo" in request.path) or ("Apriori" in request.path):
		return "Recomendaciones.html"
	elif ("acounts" in request.path) or ("update" in request.path):
		return "profile.html"
	else:
		return "index.html"
		

def sesion_manage(request):
	data={'Nickname':'Invitado','Color':'Negro'}
	sesion=True;
	id=0
	try:
		id=request.session['User_id']
	except KeyError:
		sesion=False		
	try:
		Color=request.session['Color']
	except KeyError:
		Color='Negro'
	data['Color']=Color
	if(request.method=="GET"):
		request.session['PREURL']=request.path
	else:
		request.session['PREURL']=peticion_post(request)
	if sesion:
		try:
			User=Usuario.objects.get(pk=id)
			data['Nickname']=User.Nickname
			data['Correo']=User.Correo
			data['Nombre']=User.Nombre
			data['Apellido']=User.Apellido
			data['Pais']=User.Pais
		except ObjectDoesNotExist:
			log_out(request)
	return data




def index(request):
	return render(request,'Principal/index.html',{'data': sesion_manage(request)})

def manage_static_view(request):

	str_aux=request.path[1:request.path.__len__()]
	return render(request, str_aux,{'data': sesion_manage(request)})

#falta probar
def crear_habilidades(request,User):
	Habilidad.objects.filter(User_id=User).delete()
	can=0
	for C in Categoria.objects.all():
		Nuevo_Hab=Habilidad(Categoria_id=C,User_id=User,Resueltos=0,Acumulada_num=0,Acumulada_dem=0,Nivel=0)
		Nuevo_Hab.save()
		can+=1
	User.Nivel=1
	User.Nivel_dem=can
	User.Nivel_num=0
	User.save()

def get_user(request):


	data={
		'Nombre':'',
		'Apellido':'',
		'Pais':'',
		'Nickname':'',
		'Password':'',
		'Correo':'',
		'Uva_cuenta':'',
		'Spoj_cuenta':'',
		'Codeforces_cuenta':'',
		'Coj_cuenta':''
	}
	Nombre_cuentas=['Uva','Spoj','Codeforces','Coj']
	if request.method == 'POST':
		valido=False
		if "GoogleRegister" in request.path:
			print("entrto a Google")
			form= GoogleForm(request.POST)
			print(form['Nombre'])
			print(form['Correo'])
			if(form.is_valid()):
				User=Usuario(Nombre=form.cleaned_data['Nombre'],Apellido=form.cleaned_data['Nombre'],Correo=form.cleaned_data['Correo'],Pais="Venezuela",Nickname=form.cleaned_data['Correo'],Password="GoogleUser21283057")
				print(form.cleaned_data['Nombre'])
				print(form.cleaned_data['Correo'])
				valido=True
			else:
				form = NameForm(data)
		elif "register.html" in request.path:
			form = NameForm(request.POST)
			if form.is_valid():
				User=Usuario(Nombre=form.cleaned_data['Nombre'],Apellido=form.cleaned_data['Apellido'],Correo=form.cleaned_data['Correo'],Pais=form.cleaned_data['Pais'],Nickname=form.cleaned_data['Nickname'],Password=form.cleaned_data['Password'])
				valido=True
		else:
			valido=False
		if(valido==True):
			try:
				User.validate_unique()
				User.save()
				crear_habilidades(request,User)
				request.session['User_id'] = User.id
			except ValidationError as e:
				valido=False
			if(valido):
				return HttpResponseRedirect('profile.html')
			else:
				return HttpResponseNotFound('<h1>Username o Correo ya existe</h1>')


		# if a GET (or any other method) we'll create a blank form
		else:
			form = NameForm(data)
	else:
		form = NameForm(data)
	Paises=Pais.objects.all()
	return render(request, 'Principal/register.html', {'Paises':Paises,'form': form,'data':sesion_manage(request)})



def get_login(request):
	data={'Nombre':'','Password':''}
	form = LoginForm(data)
	Valido=False
	Google=False
	if request.method == 'POST':
		print("entro1")
		print(request.path)
		if "GoogleLogin" in request.path:
			print("Entro2")
			form= GoogleForm(request.POST)
			print(request.POST)
			if form.is_valid():
				print("entro2")
				Valido=True
				try:
					User=Usuario.objects.get(Correo=form.cleaned_data['Correo'])
					Nick=User.Nickname
					Pass=User.Password
				except ObjectDoesNotExist:
					Nick=form.cleaned_data['Correo']
					Pass="GoogleUser21283057"
					Google=True
					if(Nick=="undefined"):
						return HttpResponseNotFound('<h1>No se tiene acceso a su correo intente con otro metodo</h1>')
		elif "FacebookLogin" in request.path:
			form= GoogleForm(request.POST)
			if form.is_valid():
				Valido=True
				try:
					User=Usuario.objects.get(Correo=form.cleaned_data['Correo'])
					Nick=User.Nickname
					Pass=User.Password
				except ObjectDoesNotExist:
					Nick=form.cleaned_data['Correo']
					Pass="GoogleUser21283057"
					Google=True
					if(Nick=="undefined"):
						return HttpResponseNotFound('<h1>No se tiene acceso a su correo intente con otro metodo</h1>')
		elif "signup.html" in request.path:
			form = LoginForm(request.POST)
			if form.is_valid():
				Valido=True
				Nick=form.cleaned_data['Nombre']
				Pass=form.cleaned_data['Password']
		else:
			pass
		
		if Valido==True:
			try:
				User=Usuario.objects.get(Nickname=Nick)
				if(User.Password==Pass):
					request.session['User_id'] = User.id
				else:
					return HttpResponseNotFound('<h1>Clave Incorrecta</h1>')
					Valido=False
			except ObjectDoesNotExist:
				if Google==True:
					print("ENTRO AQUI")
					if form.cleaned_data['Nombre'] == "" or form.cleaned_data['Nombre']== None:
						form.cleaned_data['Nombre']=form.cleaned_data['Correo']
					User=Usuario(Nombre=form.cleaned_data['Nombre'],Apellido=form.cleaned_data['Nombre'],Correo=form.cleaned_data['Correo'],Pais="Venezuela",Nickname=form.cleaned_data['Correo'],Password="GoogleUser21283057")
					User.save()
					crear_habilidades(request,User)
					request.session['User_id'] = User.id
					Valido=True
				else:
					return HttpResponseNotFound('<h1>Usuario no Existe</h1>')
					Valido=False
			if Valido:
				return HttpResponseRedirect('profile.html')

	return render(request, 'Principal/signup.html', {'form': form,'data':sesion_manage(request)})



#falta probar
def peor_categoria(User):
	Habilidades=Habilidad.objects.filter(User_id=User).order_by("Nivel")[0]
	return Habilidades.Categoria_id
#falta probar
def mejor_categoria(User):
	Habilidades=Habilidad.objects.filter(User_id=User).order_by("-Nivel")[0]
	return Habilidades.Categoria_id
#falta probar
def random_categoria():
	Categorias=list(Categoria.objects.all())
	pos=random.randrange(0,len(Categorias)-1)
	return Categorias[pos]

def buscar_menor_nivel(Categ,Nivel):
	Problemas=Pertenece.objects.filter(Categoria_id=Categ)
	menor=11
	dif_menor=200
	for x in Problemas:
		if(x.Problema_id.Dificultad_estadisticas!=0):
			dif=abs(x.Problema_id.Dificultad_estadisticas-Nivel)
			if(dif<=dif_menor):
				if(dif==dif_menor and x.Problema_id.Dificultad_estadisticas>=Nivel):
					dif_menor=abs(x.Problema_id.Dificultad_estadisticas-Nivel)
					menor=x.Problema_id.Dificultad_estadisticas
				elif(dif!=dif_menor):
					dif_menor=abs(x.Problema_id.Dificultad_estadisticas-Nivel)
					menor=x.Problema_id.Dificultad_estadisticas

		

	return menor

def recomendar_porcategoria(request,User,Categ,K):
	
	
	#borramos recomendaciones previas
	Prev=Resuelve.objects.filter(Usuario_id=User, Recomendado=True)
	for R in Prev:
		R.Recomendado=False
		R.save()
	
	#Obtenemos el nivel de Habilidad del usuario en la categoria
	Hab=Habilidad.objects.get(Categoria_id=Categ,User_id=User)
	if(Hab.Nivel==0):
		Nivel=buscar_menor_nivel(Categ,Hab.Nivel)
	else:
		Nivel=Hab.Nivel
	
	#primero sacamos los problemas de la categoria dada
	problemas=Pertenece.objects.filter(Categoria_id=Categ).order_by('?')
	problemas_map=set()
	for P in problemas:
		problemas_map.add(P.Problema_id.pk)
	#Ahora tenemos que tomar solo los que no se han resuelto
	No_resueltos=[]
	Resueltos=set()
	for R in Resuelve.objects.filter(Usuario_id=User,Resuelto=True):
		if(R.Problema_id.pk in problemas_map):
			Resueltos.add(R.Problema_id.pk)
	
	for P in problemas:
		if(P.Problema_id.pk not in Resueltos):
			No_resueltos.append(P.Problema_id)
		
	Recomendaciones=Recomendador().contenido(User,No_resueltos,Nivel,K)

	#ya con las recomendaciones las guardamos en la BD
	return Recomendaciones


def recomendar_colaborativo(request,User,Categ,K,Opc):

	#pedimos las recomendaciones
	Recomendaciones,Usuarios,Valores=Recomendador().colaborativo(User,Categ,K,10,Opc)
	return Recomendaciones,Usuarios,Valores


def recomendar_apriori(request,User,K,min_support,min_confidence,min_lift,max_length):
	if(max_length==0):
		max_length=None
	print("min supol 2"+str(min_support))
	Recomendaciones=Recomendador().APRIORI(User,min_support,min_confidence,min_lift,max_length,K)
	return Recomendaciones


def button_contenido(request,User):

	Problemas=[]
	form = ContenidoForm(request.POST)
	if(form.is_valid()):
		Categ_s=form.cleaned_data['Categoria']
		if(Categ_s=="Cualquiera"):
			Categ=random_categoria()
		elif(Categ_s=="Mejor"):
			Categ=mejor_categoria(User)
		elif(Categ_s=="Peor"):
			Categ=peor_categoria(User)
		else:
			Categ=Categoria.objects.get(Nombre=Categ_s)
		Problemas=recomendar_porcategoria(request,User,Categ,10)

	return Categ.Nombre,Problemas

def button_colaborativo(request,User):

	print("Estoy en colavorativo")
	Problemas=[]
	form = ColaborativoForm(request.POST)
	if(form.is_valid()):
		print("esvalido :D")
		Metodo=form.cleaned_data['Metodo']
		if(Metodo=="Coseno"):
			opc="cos"
		elif(Metodo=="Hamming"):
			opc="ham"
		elif(Metodo=="Euclidiana"):
			opc="euc"
		elif(Metodo=="BrayCurtis"):
			opc="bra"
		else:
			opc="bra"
		Problemas,Usuarios,Valores=recomendar_colaborativo(request,User,random_categoria(),15,opc)
	return Metodo,Problemas,Usuarios,Valores

def button_apriori(request,User):

	print("Estoy en apriori")
	Problemas=[]
	form = AprioriForm(request.POST)
	if(form.is_valid()):
		print("esvalido :D")
		min_support=form.cleaned_data['min_support']
		min_confidence=form.cleaned_data['min_confidence']
		min_lift = form.cleaned_data['min_lift']
		max_length = form.cleaned_data['max_length']
		print("min supol"+str(min_support))
		Problemas=recomendar_apriori(request,User,10,min_support,min_confidence,min_lift,max_length)
	return Problemas

def view_recomendaciones(request):
	str_aux=request.path[1:request.path.__len__()]
	id=-1
	try:
		id=request.session['User_id']
	except KeyError:
		return HttpResponseRedirect('signup.html')
	Tiempo_ini=time()
	User=Usuario.objects.get(pk=id)
	Categorias=Categoria.objects.all()
	Problemas1=[]
	Problemas2=[]
	Problemas3=[]
	Usuarios=[]
	Valores=[]
	Nombre_Categ='Ninguna'
	Metodo='Ninguno'
	Cantidad_P=0
	Cantidad_Pri=0
	if request.method == 'POST':
		if("Contenido" in request.path):
			Nombre_Categ,Problemas1=button_contenido(request,User)
			print("aqui saliendo tiene"+str(len(Problemas1)))
		elif("Colaborativo" in request.path):
			Metodo,Problemas2,Usuarios,Valores=button_colaborativo(request,User)
		else:
			Problemas3=button_apriori(request,User)
		#print("Mira entro")
		#form = ProblemForm(request.POST)
		#if(form.is_valid()):

	Cantidad_P=len(Problemas1)
	Cantidad_Pri=len(Problemas3)
	Tiempo_fin=time()
	Tiempo=Tiempo_fin-Tiempo_ini
	return render(request, 'Principal/Recomendaciones.html',{'data': sesion_manage(request),'Tiempo':Tiempo,'Nombre_Metodo':Metodo,'Usuarios':Usuarios,'Valores':Valores,'Nombre_Categ':Nombre_Categ,'Cantidad_P':Cantidad_P,'Cantidad_Pri':Cantidad_Pri,'Categorias':Categorias,'Problemas1':Problemas1,'Problemas2':Problemas2,'Problemas3':Problemas3})


def view_profile(request):
	Nombre_cuentas=['Uva','Spoj','Codeforces','Coj']
	data_Cuenta={
		'Uva_cuenta':'',
		'Spoj_cuenta':'',
		'Codeforces_cuenta':'',
		'Coj_cuenta':''
	}
	Habilidades={
		'General':0.0,
		'por_categorias':[]
	}

	str_aux=request.path[1:request.path.__len__()]
	id=-1
	try:
		id=request.session['User_id']
	except KeyError:
		return HttpResponseRedirect('signup.html')
	frag=6
	User=Usuario.objects.get(pk=id)
	Problemas=Resuelve.objects.filter(Usuario_id=User,Resuelto=True)
	Total=len(Problemas)
	Problemas=fragmentar(Problemas,frag)
	Noclasificados=Resuelve.objects.filter(Usuario_id=User,Resuelto=True,Clasificado=False)[:24]
	Noclasificados=fragmentar(Noclasificados,frag)
	Recomendados=Resuelve.objects.filter(Usuario_id=User,Resuelto=False,Recomendado=True)[:10]
	Recomendados=fragmentar(Recomendados,frag)
	Habilidades['por_categorias']=fragmentar(Habilidad.objects.filter(User_id=User),3)
	
	Habilidades['General']=User.Nivel
	for ID in Nombre_cuentas:
		user_cuenta=ID+'_cuenta'
		Judge=Juez.objects.get(Nombre=ID)
		try:
			Acount=Cuenta.objects.get(Usuario_id=User,Juez_id=Judge)
			data_Cuenta[user_cuenta]=Acount.ID_cuenta
		except ObjectDoesNotExist:
			data_Cuenta[user_cuenta]=''

	form = CuentaForm(data_Cuenta)
	return render(request, str_aux,{'data': sesion_manage(request),'Total':Total,'problemas':Problemas,'form': form,'noclasi':Noclasificados,'recomendados':Recomendados,'Habilidades':Habilidades})


def dame_categoria(problem):

	Obj=Pertenece.objects.filter(Problema_id=problem)
	if(Obj):
		Obj=Obj.order_by('-Votos')[0]
		return Obj.Categoria_id
	return None


#falta probar
def sumar_habilidad(User,Categ,Dif):
	if Dif!=None and Dif!=0:
		Hab=Habilidad.objects.get(User_id=User,Categoria_id=Categ)
		User.Nivel_num-=float(Hab.Nivel)
		Hab.Resueltos+=1
		Hab.Acumulada_num+=float(float(Dif)*float(Dif))
		Hab.Acumulada_dem+=float(Dif)
		Hab.Nivel = Hab.Acumulada_num/Hab.Acumulada_dem
		Hab.save()
		User.Nivel_num+=float(Hab.Nivel)
		User.Nivel=Decimal(User.Nivel_num/User.Nivel_dem)
		User.save()

#falta probar
def dar_puntuacion(User,Problem):
	Dif=Problem.Dificultad_estadisticas
	if Dif!=None and Dif>0:
		Categorias=Pertenece.objects.filter(Problema_id=Problem)
		if(Categorias):
			Categorias.order_by("-Votos")
			print (Categorias)
			Mayor=Categorias[0].Votos
			for C in Categorias:
				Hab=Habilidad.objects.get(User_id=User,Categoria_id=C.Categoria_id)
				User.Nivel_num-=float(Hab.Nivel)
				Hab.Resueltos+=1
				pond=(C.Votos*float(Dif))/Mayor
				Hab.Acumulada_num+=float(float(Dif)*pond)
				Hab.Acumulada_dem+=pond
				Hab.Nivel = Hab.Acumulada_num/Hab.Acumulada_dem
				Hab.save()
				User.Nivel_num+=float(Hab.Nivel)
				User.Nivel=Decimal(User.Nivel_num/User.Nivel_dem)
				User.save()
				


def guardar_problemas(User,con):
	cuentas=Cuenta.objects.filter(Usuario_id=User)
	for C in cuentas:
		Crawler_juez=C.Juez_id.Crawler
		J=C.Juez_id
		name_acount=C.ID_cuenta
		query=['scrapy','crawl',Crawler_juez+'_user',"-a","usuario="+name_acount,"-a","con="+str(con)]
		subprocess.run(args=query)
		#time.sleep(5)
		filename="data/"+Crawler_juez+"_"+name_acount+".data"
		try:
			with open(filename, "r",encoding="utf-8", errors="surrogateescape") as f:
				linea=f.readline()
				while linea!="":
					Lista=linea.split()
					Nombre=Lista[0]
					dif=Decimal(float(Lista[1]))
					Descripcion=f.readline().split("\n")[0]
					URL=f.readline().split("\n")[0]
					tam=int(f.readline().split("\n")[0])
					try:
						OBJ=Problema.objects.get(Nombre=Nombre,URL=URL)
						if(tam>0):
							Lista=f.readline().split()
					except ObjectDoesNotExist:
						OBJ=Problema(Nombre=Nombre,URL=URL,Descripcion=Descripcion,Juez_id=J,Dificultad_estadisticas=dif)
						OBJ.save()
						if(tam>0):
							Lista=f.readline().split()
							for categoria in Lista:
								for C in Categoria.objects.all():
									if(categoria in C.Otros):
										Relacion = Pertenece(Problema_id=OBJ,Categoria_id=C,Votos=20)
										Relacion.save()
										break
					#aqui ya esta seguro que el problema esta guardadoen la BD
					#por tanto guardamos los resuelve
					nivel=1
					if(User.Nivel!=None):
						nivel=User.Nivel
					try:
						AUX=Resuelve.objects.get(Usuario_id=User, Problema_id=OBJ)
						if(AUX.Resuelto!=True):
							dar_puntuacion(User,OBJ)
						AUX.Resuelto=True
						AUX.Recomendado=False
						AUX.save()
					except ObjectDoesNotExist:
						AUX=Resuelve(Usuario_id=User, Problema_id=OBJ,Juez_id=J,Resuelto=True,Nivel_resuelto=nivel,Recomendado=False)
						AUX.save()
						dar_puntuacion(User,OBJ)
					linea=f.readline()
		except OSError:
			pass
			


def update_acounts(request):
	Nombre_cuentas=['Uva','Spoj','Codeforces','Coj']
	id=-1
	try:
		id=request.session['User_id']
	except KeyError:
		return HttpResponseRedirect('signup.html')

	User=Usuario.objects.get(pk=id)	
	
	if request.method == 'POST':
		form = CuentaForm(request.POST)
		for ID in Nombre_cuentas:
			Judge=Juez.objects.get(Nombre=ID)
			Resuelve.objects.filter(Usuario_id=User,Juez_id=Judge).delete()
			user_cuenta=ID+'_cuenta'
			if(form.is_valid()):
				if(form.cleaned_data[user_cuenta]!=''):
					print(form.cleaned_data[user_cuenta])
					print(form["Uva_cuenta"])
					try:
						Acount=Cuenta.objects.get(Usuario_id=User,Juez_id=Judge)
						Acount.delete()
					except ObjectDoesNotExist:
						pass
					Cuenta_Nueva=Cuenta(Usuario_id=User,Juez_id=Judge,ID_cuenta=form.cleaned_data[user_cuenta],Problemas_resueltos=0,Problemas_pendientes=0)
					Cuenta_Nueva.save()

	crear_habilidades(request,User)
	guardar_problemas(User,10000)
	return HttpResponseRedirect('profile.html')


def update_problems(request):

	str_aux=request.path[1:request.path.__len__()]
	id=-1
	try:
		id=request.session['User_id']
	except KeyError:
		return HttpResponseRedirect('signup.html')

	User=Usuario.objects.get(pk=id)
	guardar_problemas(User,100)

	return HttpResponseRedirect('profile.html')

def dar_paginas(Num_pag,Limite):
	print(str(Limite)+" VS "+str(Num_pag))
	if Limite==0:
		return []
	if Limite<=8:
		return [i+1 for i in range(Limite)]
	
	if Num_pag<=4:
		return [i+1 for i in range(8)]
	if Limite-Num_pag<=4:
		print("MIRA")
		print([Limite-i for i in range(8)][::-1])
		return [Limite-i for i in range(8)][::-1]
	return range(Num_pag-3,Num_pag+5)

	
def fragmentar(Lista,tam):
	n=len(Lista)
	div=n//tam
	mod=n%tam
	if n%tam!=0:
		div=div+1
	Nlista=[]
	for i in range(div):
		max=tam
		if i==div-1 and mod!=0:
			max=mod
		Nlista.append([])
		for j in range(max):
			Nlista[i].append(Lista[i*tam+j])
	return Nlista
	

def view_problemas3(request):
	str_aux=request.path[1:request.path.__len__()]
	data_inicial={'Juez':request.session['Juez'],'Categoria':request.session['Categoria'],'Dificultad_min':request.session['Dificultad_min'],'Dificultad_max':request.session['Dificultad_max']}
	form = ProblemForm(data_inicial)
	pagina_id=request.session['Pag_actual']
	
	if "Prim" in request.path:
		pagina_id=1
	if "Ult" in request.path:
		pagina_id=request.session['Pag_lim']
	if "Next" in request.path and pagina_id<request.session['Pag_lim']:
		pagina_id+=1
	if "Prev" in request.path and pagina_id>1:	
		pagina_id-=1
	
	request.session['Pag_actual']=pagina_id
	pagina_id-=1
	Categorias=Categoria.objects.all()
	Jueces=Juez.objects.all()
	Problemas=[]
	LI=request.session['Dificultad_min']
	LS=request.session['Dificultad_max']
	J=request.session['Juez']
	C=request.session['Categoria']
	Total=102
	frag=6
	Total2=0
	print("Total "+str(pagina_id*Total))
	print(str(LI)+" "+str(LS)+" "+J+" "+C)
	if(J=='Todos'):
		if(C=='Todas'):
			Problemas=Problema.objects.filter(Dificultad_estadisticas__gte=LI,Dificultad_estadisticas__lte=LS)
			Total2=Problemas.count()
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
		else:
			Cate=Categoria.objects.get(Nombre=C)
			Perte=Pertenece.objects.filter(Categoria_id=Cate)
			for P in Perte:
				dif=P.Problema_id.Dificultad_estadisticas
				if dif!=None and dif>=LI and dif<=LS:
					Problemas.append(P.Problema_id)
			Total2=len(Problemas)
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
	else:
		if(C=='Todas'):
			Tjuez=Juez.objects.get(Nombre=J)
			Problemas=Problema.objects.filter(Juez_id=Tjuez,Dificultad_estadisticas__gte=LI,Dificultad_estadisticas__lte=LS)
			Total2=Problemas.count()
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
		else:
			Cate=Categoria.objects.get(Nombre=C)
			Perte=Pertenece.objects.filter(Categoria_id=Cate)
			Tjuez=Juez.objects.get(Nombre=J)
			for P in Perte:
				dif=P.Problema_id.Dificultad_estadisticas
				if dif!=None and dif>=LI and dif<=LS and P.Problema_id.Juez_id==Tjuez:
					Problemas.append(P.Problema_id)
			Total2=len(Problemas)
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
	div=Total2//Total
	mod=Total2%Total
	if(mod!=0):
		tam=div+1
	else:
		tam=div
	Paginas=dar_paginas(request.session['Pag_actual'],request.session['Pag_lim'])
	Actual=request.session['Pag_actual']
	Jue=request.session['Juez']
	Catego=request.session['Categoria']
	Muestra=True
	Problemas=fragmentar(Problemas,frag)
	return render(request, 'Principal/problemas.html',{'data': sesion_manage(request),'Categorias':Categorias,'Jueces':Jueces,'Problemas':Problemas,'Paginas':Paginas,'Actual':Actual,'Juez':Jue,'Categoria':Catego,'Muestra':Muestra,'form':form})

def view_problemas2(request,pagina_id=10):
	str_aux=request.path[1:request.path.__len__()]
	data_inicial={'Juez':request.session['Juez'],'Categoria':request.session['Categoria'],'Dificultad_min':request.session['Dificultad_min'],'Dificultad_max':request.session['Dificultad_max']}
	form = ProblemForm(data_inicial)
	pagina_id=int(pagina_id)
	request.session['Pag_actual']=pagina_id
	pagina_id-=1
	Categorias=Categoria.objects.all()
	Jueces=Juez.objects.all()
	Problemas=[]
	LI=request.session['Dificultad_min']
	LS=request.session['Dificultad_max']
	J=request.session['Juez']
	C=request.session['Categoria']
	Total=102
	frag=6
	Total2=0
	print("Total "+str(pagina_id*Total))
	print(str(LI)+" "+str(LS)+" "+J+" "+C)
	if(J=='Todos'):
		if(C=='Todas'):
			Problemas=Problema.objects.filter(Dificultad_estadisticas__gte=LI,Dificultad_estadisticas__lte=LS)
			Total2=Problemas.count()
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
		else:
			Cate=Categoria.objects.get(Nombre=C)
			Perte=Pertenece.objects.filter(Categoria_id=Cate)
			for P in Perte:
				dif=P.Problema_id.Dificultad_estadisticas
				if dif!=None and dif>=LI and dif<=LS:
					Problemas.append(P.Problema_id)
			Total2=len(Problemas)
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
	else:
		if(C=='Todas'):
			Tjuez=Juez.objects.get(Nombre=J)
			Problemas=Problema.objects.filter(Juez_id=Tjuez,Dificultad_estadisticas__gte=LI,Dificultad_estadisticas__lte=LS)
			Total2=Problemas.count()
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
		else:
			Cate=Categoria.objects.get(Nombre=C)
			Perte=Pertenece.objects.filter(Categoria_id=Cate)
			Tjuez=Juez.objects.get(Nombre=J)
			for P in Perte:
				dif=P.Problema_id.Dificultad_estadisticas
				if dif!=None and dif>=LI and dif<=LS and P.Problema_id.Juez_id==Tjuez:
					Problemas.append(P.Problema_id)
			Total2=len(Problemas)
			Problemas=Problemas[pagina_id*Total:pagina_id*Total+Total]
	div=Total2//Total
	mod=Total2%Total
	if(mod!=0):
		tam=div+1
	else:
		tam=div
	Paginas=dar_paginas(request.session['Pag_actual'],request.session['Pag_lim'])
	Actual=request.session['Pag_actual']
	Jue=request.session['Juez']
	Catego=request.session['Categoria']
	Muestra=True
	Problemas=fragmentar(Problemas,frag)
	return render(request, 'Principal/problemas.html',{'data': sesion_manage(request),'Categorias':Categorias,'Jueces':Jueces,'Problemas':Problemas,'Paginas':Paginas,'Actual':Actual,'Juez':Jue,'Categoria':Catego,'Muestra':Muestra,'form':form})

def view_problemas(request):
	str_aux=request.path[1:request.path.__len__()]
	Muestra=True
	if(request.method=='GET'):
		Muestra=False
		print("entroooooooooooo")
		request.session['Juez']='Todos'
		request.session['Categoria']='Todas'
		request.session['Dificultad_min']=1.0
		request.session['Dificultad_max']=10.0
		try:
			del request.session['Pag_lim']
			del request.session['Pag_actual']
		except KeyError:
			pass
	else:
		Muestra=True
	
	data_inicial={'Juez':request.session['Juez'],'Categoria':request.session['Categoria'],'Dificultad_min':request.session['Dificultad_min'],'Dificultad_max':request.session['Dificultad_max']}
	
	Categorias=Categoria.objects.all()
	Jueces=Juez.objects.all()
	Problemas=[]
	Paginas=[]
	form = ProblemForm(data_inicial)
	Total2=0
	Total=102
	frag=6
	if request.method == 'POST':
		print("Mira entro")
		form = ProblemForm(request.POST)
		if(form.is_valid()):
			request.session['Juez']=form.cleaned_data['Juez']
			request.session['Categoria']=form.cleaned_data['Categoria']
			request.session['Dificultad_min']=form.cleaned_data['Dificultad_min']
			request.session['Dificultad_max']=form.cleaned_data['Dificultad_max']
			LI=form.cleaned_data['Dificultad_min']
			LS=form.cleaned_data['Dificultad_max']
			J=form.cleaned_data['Juez']
			C=form.cleaned_data['Categoria']
			print("Total "+str(Total))
			print(str(LI)+" "+str(LS)+" "+J+" "+C)
			if(J=='Todos'):
				if(C=='Todas'):
					Problemas=Problema.objects.filter(Dificultad_estadisticas__gte=LI,Dificultad_estadisticas__lte=LS)
					Total2=Problemas.count()
					Problemas=Problemas[:Total]
				else:
					Cate=Categoria.objects.get(Nombre=C)
					Perte=Pertenece.objects.filter(Categoria_id=Cate)
					for P in Perte:
						dif=P.Problema_id.Dificultad_estadisticas
						if dif!=None and dif>=LI and dif<=LS:
							Problemas.append(P.Problema_id)
					Total2=len(Problemas)
					Problemas=Problemas[:Total]
			else:
				if(C=='Todas'):
					Tjuez=Juez.objects.get(Nombre=J)
					Problemas=Problema.objects.filter(Juez_id=Tjuez,Dificultad_estadisticas__gte=LI,Dificultad_estadisticas__lte=LS)
					Total2=Problemas.count()
					Problemas=Problemas[:Total]
				else:
					Cate=Categoria.objects.get(Nombre=C)
					Perte=Pertenece.objects.filter(Categoria_id=Cate)
					Tjuez=Juez.objects.get(Nombre=J)
					for P in Perte:
						dif=P.Problema_id.Dificultad_estadisticas

						if dif!=None and dif>=LI and dif<=LS and P.Problema_id.Juez_id==Tjuez:
							Problemas.append(P.Problema_id)
					Total2=len(Problemas)
					Problemas=Problemas[:Total]
	
	div=Total2//Total
	mod=Total2%Total
	if(mod!=0):
		tam=div+1
	else:
		tam=div
	request.session['Pag_actual']=1
	request.session['Pag_lim']=tam
	Paginas=dar_paginas(request.session['Pag_actual'],request.session['Pag_lim'])
	Actual=request.session['Pag_actual']
	Jue=request.session['Juez']
	Catego=request.session['Categoria']
	Problemas=fragmentar(Problemas,frag)
	return render(request, 'Principal/problemas.html',{'data': sesion_manage(request),'Categorias':Categorias,'Jueces':Jueces,'Problemas':Problemas,'Paginas':Paginas,'Actual':Actual,'Juez':Jue,'Categoria':Catego,'Muestra':Muestra,'form':form})



def procesar_clasificacion(request,User,Problem):
	print("Entro")
	try:
		Votos=Resuelve.objects.get(Usuario_id=User,Problema_id=Problem,Resuelto=True,Clasificado=True)
	except ObjectDoesNotExist:
		return False
	print("excepciono")
	Dif_asig=Votos.Dificultad_asig
	Nivel_res=Votos.Nivel_resuelto
	print(type(Problem))
	Problem.Dificultad_usuarios_num+=float(Dif_asig)
	Problem.Dificultad_usuarios_dem+=1
	Problem.Dificultad_resuelto_num+=float(Nivel_res)
	Problem.Dificultad_resuelto_dem+=1
	Problem.Dificultad_usuarios=Decimal(Problem.Dificultad_usuarios_num/Problem.Dificultad_usuarios_dem)
	Problem.Dificultad_resuelto=Decimal(Problem.Dificultad_resuelto_num/Problem.Dificultad_resuelto_dem)
	Problem.save()
	print("proceso problema")
	Cate_asig=[Votos.Categoria1,Votos.Categoria2,Votos.Categoria3]
	Puntos=[3,2,1]


	print("Ahora va por las categorias")
	for i in range(3):
		Categ=Cate_asig[i]
		if(Categ!=None and Categ!=""):
			Cate=Categoria.objects.get(Nombre=Categ)
			try:
				OBJ=Pertenece.objects.get(Problema_id=Problem,Categoria_id=Cate)
				OBJ.Votos+=Puntos[i]
				OBJ.save()
			except ObjectDoesNotExist:
				OBJ=Pertenece(Problema_id=Problem,Categoria_id=Cate,Votos=Puntos[i])
				OBJ.save()
	print("Categorizo")
	"""
	Cate=Pertenece.objects.filter(Problema_id=Problem)
	if len(Cate)>0:
		Cate=Cate.order_by('-Votos')[0]
		Hab=
		.objects.get(User_id=User,Categoria_id=Cate.Categoria_id)
		if Problem.Dificultad_estadisticas!=None and Problem.Dificultad_estadisticas!=0:
			Hab.Resueltos+=1
			Hab.Acumulada_num+=float(float(Problem.Dificultad_estadisticas)*float(Problem.Dificultad_estadisticas))
			Hab.Acumulada_dem+=float(Problem.Dificultad_estadisticas)
			Hab.Nivel=Hab.Acumulada_num/Hab.Acumulada_dem

	habilidad_general(request,User)
	"""
	return True

def view_clasifica(request,problem_id):



	str_aux=request.path[1:request.path.__len__()]
	id=-1
	try:
		id=request.session['User_id']
	except KeyError:
		return HttpResponseRedirect('signup.html')

	User=Usuario.objects.get(pk=id)

	Problem=Problema.objects.get(pk=problem_id)

	form = ClasificacionForm()
	if request.method == 'POST':
		form = ClasificacionForm(request.POST)
		Objeto=Resuelve.objects.get(Usuario_id=User,Problema_id=Problem)
		if(form.is_valid()):
			Objeto.Dificultad_asig=form.cleaned_data['Dificultad']
			Objeto.Categoria1=form.cleaned_data['Categoria1']
			if(form.cleaned_data['Categoria2']!=''):
				Objeto.Categoria2=form.cleaned_data['Categoria2']
			if(form.cleaned_data['Categoria3']!=''):
				Objeto.Categoria3=form.cleaned_data['Categoria3']
			Objeto.Clasificado=True
			Objeto.save()
			procesar_clasificacion(request,User,Problem)
			return HttpResponseRedirect('/Principal/profile.html')
		else:
			return HttpResponseNotFound("<h1>mierda "+str(form['Dificultad'])+"</h1>")



	data_Problem={'Nombre':'','Descripcion':'','Dificultad':0,'URL':''}
	try:
		Problem=Problema.objects.get(pk=problem_id)
		Objeto=Resuelve.objects.get(Usuario_id=User,Problema_id=problem_id)
		if(Objeto.Clasificado==True):
			return HttpResponseNotFound('<h1>Ya se Clasifico el Problema</h1>')
	except ObjectDoesNotExist:
		return HttpResponseNotFound('<h1>Page not found</h1>')
	Categorias=Categoria.objects.all()
	return render(request, 'Principal/Clasificacion.html',{'data': sesion_manage(request),'data_Problem':Problem,'Categorias':Categorias,'form':form})

def view_Clasificacion(request):
	return HttpResponseRedirect('profile.html')


def calcular_voto_problema(request,Problem):

	Votos=Resuelve.objects.filter(Problema_id=Problem,Resuelto=True,Clasificado=True)
	for V in Votos:
		Cate_asig=[V.Categoria1,V.Categoria2,V.Categoria3]
		Puntos=[3,2,1]
		for i in range(3):
			Categ=Cate_asig[i]
			if(Categ!=None and Categ!=""):
				try:
					OBJ=Pertenece.objects.get(Problema_id=Problem,Categoria_id=Categ)
					OBJ.Votos+=Puntos[i]
					OBJ.save()
				except ObjectDoesNotExist:
					OBJ=Pertenece(Problema_id=Problem,Categoria_id=Categ)
					OBJ.save()

def calcular_votos_todos(request):
	Problemas=Problema.objects.all()
	for P in Problemas:
		calcular_voto_problema(request,P)



def reasignar_categorias_problema(request,Problem):
	Pertenencias=Pertenece.objects.filter(Problema_id=Problem)
	if(Pertenencias):
		Pertenencias=Pertenencias.order_by("-Votos")
		tam=len(Pertenencias)
		i=0
		for P in Pertenencias:
			if(i<3):
				continue
			else:
				P.delete()

def reasignar_categorias_problemas(request):
	Problemas=Problema.objects.all()
	for P in Problemas:
		reasignar_categorias_problema(request,P)




def calcular_dificultad(request,User):

	Problemas=Problema.objects.all()
	print("HOLA")
	for P in Problemas:
		Votos=Resuelve.objects.filter(Problema_id=P,Resuelto=True)
		can=0
		Promedio=Decimal(0)
		Nivel_res=Decimal(0)
		can2=0
		for X in Votos:
			Nivel_res=Nivel_res+X.Nivel_resuelto
			can2=can2+1
			if X.Clasificado:
				Promedio=Promedio+X.Dificultad_asig
				can=can+1
		#print("QLOQ")
		if can2!=0:
			Nivel_res=Nivel_res/can2
			P.Dificultad_resuelto=Nivel_res
			P.Dificultad_resuelto_num=Nivel_res
			P.Dificultad_resuelto_dem=can2
		if can<=0:
			Promedio=P.Dificultad_estadisticas
		else:
			Promedio=Promedio/can
			P.Dificultad_usuarios_num=Promedio
			P.Dificultad_usuarios_dem=can
			P.Dificultad_usuarios=Promedio

		#print(str(can)+" "+str(can2)+" "+str(P.Votos_asig)+" "+str(P.Votos_nivel))
		if (can>0 and P.Votos_asig!=can) or(can2>0 and P.Votos_nivel!=can2):
			P.Votos_asig=can
			P.Votos_nivel=can2
			print(str(P.pk))
			P.save()

def habilidad_general(request,User):
	Habilidades=Habilidad.objects.filter(User_id=User)
	Nivel=0
	can=0
	for H in Habilidades:
		Nivel+=H.Nivel
		can+=1
	User.Nivel=Nivel/can
	User.save()




def calcular_habilidad(request,User):

	Habilidades=Habilidad.objects.filter(User_id=User)
	if(Habilidades):
		Habilidades.delete()

	Categorias=Categoria.objects.all()

	for C in Categorias:
		Nuevo_Hab=Habilidad(Categoria_id=C,User_id=User,Resueltos=0)
		Nuevo_Hab.save()

	PResueltos=Resuelve.objects.filter(Usuario_id=User,Resuelto=True)
	for P in PResueltos:
		Cate=Pertenece.objects.filter(Problema_id=P.Problema_id)
		if len(Cate)>0:
			Cate=Cate.order_by('-Votos')[0]
			Hab=Habilidad.objects.get(User_id=User,Categoria_id=Cate.Categoria_id)
			if P.Problema_id.Dificultad_estadisticas!=None and P.Problema_id.Dificultad_estadisticas!=0:
				Hab.Resueltos+=1
				Hab.Acumulada_num+=float(float(P.Problema_id.Dificultad_estadisticas)*float(P.Problema_id.Dificultad_estadisticas))
				Hab.Acumulada_dem+=float(P.Problema_id.Dificultad_estadisticas)
				Hab.Nivel=Hab.Acumulada_num/Hab.Acumulada_dem
				Hab.save()

	habilidad_general(request,User)



def update_habilidad(request):
	str_aux=request.path[1:request.path.__len__()]
	id=-1
	try:
		id=request.session['User_id']
	except KeyError:
		return HttpResponseRedirect('signup.html')
	print("Empezara a updatear")
	User=Usuario.objects.get(pk=id)
	calcular_habilidad(request,User)
	print("Termino de updatear")
	return HttpResponseRedirect('profile.html')
	
	
def view_recomendar(request):

	str_aux=request.path[1:request.path.__len__()]
	id=-1
	try:
		id=request.session['User_id']
	except KeyError:
		return HttpResponseRedirect('signup.html')

	User=Usuario.objects.get(pk=id)
	Recom=Resuelve.objects.filter(Usuario_id=User,Recomendado=True,Resuelto=False)
	if(Recom):
		Recom.delete()
	canti=0
	Mejor_catg=mejor_categoria(User)
	Recomendaciones=(recomendar_porcategoria(request,User,Mejor_catg,3))
	Peor_catg=peor_categoria(User)
	Recomendaciones.extend(recomendar_porcategoria(request,User,Peor_catg,4))
	canti+=len(Recomendaciones)
	random_catg=random_categoria()
	Recomendaciones.extend(recomendar_porcategoria(request,User,random_catg,10-canti))
	for R in Recomendaciones:
		try:
			Objeto=Resuelve.objects.get(Usuario_id=User,Problema_id=R)
			Objeto.Recomendado=True
			Objeto.save()
		except ObjectDoesNotExist:
			Objeto=Resuelve(Usuario_id=User,Problema_id=R,Juez_id=R.Juez_id,Recomendado=True)
			Objeto.save()

	return HttpResponseRedirect('profile.html')

def view_tutoriales(request):
	str_aux=request.path[1:request.path.__len__()]

	resul=Tutorial.objects.all()
	tutoriales=fragmentar(resul,3)


	return render(request, str_aux,{'data': sesion_manage(request),'tutoriales':tutoriales})

