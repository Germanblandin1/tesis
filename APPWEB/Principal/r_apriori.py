from apyori import apriori
import random

#transactions: lista de transacciones que las leeré de un dataframe pero necesitamos una forma de obtenerla primero
# los niveles deben ir de 1-10 y los problemas de 11-n para no mezclar ajusta eso en la BD

#nivel: nivel del usuario
#resueltos: problemas resueltos por el usuario


#la confianza,el soporte y el lift (que no se que es todavia veré mañana) se modifican en apyori.py

def ApRe(nivel,resueltos):

	transactions = [
		['2','121'],
		['3','121'],
		['1','100'],
		['2','121'],
		['1','100'],
	]

	results = list(apriori(transactions))
	random.seed(a=None)
	
	selected = 0
	tam_res=len(results)-1 #tamaño del resultado final
	tam_prob=10000 #numero de problemas en la BD
	iteraciones=0
	while (selected==0):
		pos = random.randrange(max(0,tam_res-20),tam_res) #se generaran 10 grupos aproximadamente dificultades el -20 es por si acaso
		problema = random.randrange(11,tam_prob) #un problema random entre todos los problemas
		iteraciones=iteraciones+1 #iteraciones para que no busque para siempre
		if (not (resueltos[problema]) ): #si no fue resuelto
			if(problema in results[pos]): #si el problema está en el conjunto
				if ( (nivel in results[pos]) or ((nivel+1) in results[pos]) or ((nivel) in results[pos])): #si el nivel está en el conjunto o el nivel+1
					#el problema existe y esta en el nivel del usuario con un buen grado de confianza
					return problema
		if (iteraciones==8000):
			#no se encontró un buen match en los intentos asi que se retorna el problema
			return problema