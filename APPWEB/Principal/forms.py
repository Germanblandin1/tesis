from django import forms

class NameForm(forms.Form):
	Nombre = forms.CharField(label='Nombre', max_length=100)
	Apellido = forms.CharField(label='Apellido', max_length=100)
	Pais = forms.CharField(label='Pais', max_length=100)
	Nickname = forms.CharField(label='Username', max_length=100)
	Password = forms.CharField(label='Password', max_length=100)
	Correo = forms.EmailField(label='Correo', max_length=100)
	
	Uva_cuenta = forms.CharField(label='Uva_cuenta', max_length=100, required=False)
	Spoj_cuenta = forms.CharField(label='Spoj_cuenta', max_length=100, required=False)
	Codeforces_cuenta = forms.CharField(label='Codeforces_cuenta', max_length=100, required=False)
	Coj_cuenta= forms.CharField(label='Coj_cuenta', max_length=100, required=False)

class LoginForm(forms.Form):
	Nombre = forms.CharField(label='Usuario', max_length=100)
	Password = forms.CharField(label='Password', max_length=100)

class CuentaForm(forms.Form):	
	Uva_cuenta = forms.CharField(label='Uva_cuenta', max_length=100, required=False)
	Spoj_cuenta = forms.CharField(label='Spoj_cuenta', max_length=100, required=False)
	Codeforces_cuenta = forms.CharField(label='Codeforces_cuenta', max_length=100, required=False)
	Coj_cuenta= forms.CharField(label='Coj_cuenta', max_length=100, required=False)
	
class ClasificacionForm(forms.Form):
	Dificultad = forms.FloatField(label='Dificultad')
	Categoria1 = forms.CharField(label='Categoria1')
	Categoria2 = forms.CharField(label='Categoria2',required=False)
	Categoria3 = forms.CharField(label='Categoria3',required=False)

class ProblemForm(forms.Form):
	Dificultad_min = forms.FloatField(label='Dificultad_min',required=False)
	Dificultad_max = forms.FloatField(label='Dificultad_max',required=False)
	Juez = forms.CharField(label='Juez',required=False)
	Categoria = forms.CharField(label='Categoria',required=False)
	
class ContenidoForm(forms.Form):
	Categoria = forms.CharField(label='Categoria',required=True)

class ColaborativoForm(forms.Form):
	Metodo = forms.CharField(label='Metodo',required=True)

class AprioriForm(forms.Form):
	min_support = forms.FloatField(label='min_support',required=True)
	min_confidence = forms.FloatField(label='min_confidence',required=True)
	min_lift = forms.FloatField(label='min_lift',required=False)
	max_length = forms.IntegerField(label='max_length',required=False)

class GoogleForm(forms.Form):
	Nombre = forms.CharField(label='Nombre', max_length=100,required=False)
	Correo = forms.EmailField(label='Correo', max_length=100,required=True)