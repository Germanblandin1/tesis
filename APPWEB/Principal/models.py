from django.db import models

# Create your models here.


class Pais(models.Model):
	Nombre=models.CharField(max_length=100)
	def __str__(self):
		return self.Nombre

class Juez(models.Model):
	Nombre=models.CharField(max_length=45)
	Prefijo=models.CharField(max_length=3,blank=True)
	URL=models.URLField(unique=True)
	Crawler=models.CharField(max_length=45,blank=True)
	
	def __str__(self):
		return self.Nombre
	
class Categoria(models.Model):
	Nombre=models.CharField(max_length=45,unique=True)
	Dificultad_categoria=models.DecimalField(blank=True,null=True,default=0,max_digits=3, decimal_places=1)
	Otros=models.TextField(null=True)
	def __str__(self):
		return self.Nombre
		

class Problema(models.Model):
	Nombre=models.CharField(max_length=45)
	URL=models.URLField(unique=True)
	Descripcion=models.TextField(blank=True)
	Dificultad_estadisticas=models.DecimalField(default=0, max_digits=3, decimal_places=1)
	Dificultad_usuarios=models.DecimalField(default=0, max_digits=3, decimal_places=1)
	Dificultad_usuarios_num = models.FloatField(default=0.0)
	Dificultad_usuarios_dem = models.FloatField(default=0.0)
	Dificultad_resuelto=models.DecimalField(default=0, max_digits=3, decimal_places=1)
	Dificultad_resuelto_num = models.FloatField(default=0.0)
	Dificultad_resuelto_dem = models.FloatField(default=0.0)
	Votos_asig=models.IntegerField(default=0)
	Votos_nivel=models.IntegerField(default=0)
	
	
	#relaciones
	Juez_id = models.ForeignKey(Juez, on_delete=models.CASCADE)
	Categorias = models.ManyToManyField(Categoria, through='Pertenece')
	
	def __str__(self):
		return str(self.Juez_id)+"."+self.Nombre+"."+str(self.pk)
	


class Usuario(models.Model):
	#Atributos de la clase
	Nombre = models.CharField(max_length=45)
	Apellido = models.CharField(max_length=45)
	Pais = models.CharField(max_length=45)
	Nickname=models.CharField(max_length=60, unique=True)
	Password = models.CharField(max_length=45,default="123456")
	Correo=models.EmailField(unique=True)
	Nivel = models.DecimalField(default=1,max_digits=3, decimal_places=1)
	Nivel_num = models.FloatField(default=0.0)
	Nivel_dem = models.FloatField(default=0.0)
	#relaciones
	Cuentas = models.ManyToManyField(Juez, through='Cuenta')  #establece relacion muchos a muchos con Juez
	Problemas = models.ManyToManyField(Problema, through='Resuelve')
	Habilidades = models.ManyToManyField(Categoria, through='Habilidad')
	#Votos2 = models.ManyToManyField(Problema, through='Voto')
	#Metodos
	
	#Metodos "Constructores" que vienen defecto de python y sirve para rellenar los atributos
	def __str__(self):
		return self.Nickname

class Idioma(models.Model):
	Nombre=models.CharField(max_length=45,unique=True)
	
	def __str__(self):
		return self.Nombre
		
		
	
class Tutorial(models.Model):
	Descripcion=models.TextField(blank=True)
	URL=models.URLField(unique=True)
	Imagen=models.ImageField(upload_to="Tutoriales",null=True)
	#relaciones
	Categoria_id = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	Idioma_id = models.ForeignKey(Idioma, on_delete=models.CASCADE,null=True)
	def __str__(self):
		return self.Descripcion


#Relacion entre Usuario y Juez sirve para guardar las cuentas del usuario
class Cuenta(models.Model):
	Usuario_id = models.ForeignKey(Usuario, on_delete=models.CASCADE)
	Juez_id = models.ForeignKey(Juez, on_delete=models.CASCADE)
	ID_cuenta=models.CharField(max_length=45)
	Problemas_resueltos =models.IntegerField(default=0)
	Problemas_pendientes =models.IntegerField(default=0)
	
	def __str__(self):
		return str(self.Usuario_id)+"-"+str(self.Juez_id)

#relacion entre usuario y los problemas
class Resuelve(models.Model):
	Usuario_id = models.ForeignKey(Usuario, on_delete=models.CASCADE)
	Problema_id = models.ForeignKey(Problema, on_delete=models.CASCADE)
	Juez_id = models.ForeignKey(Juez, on_delete=models.CASCADE,null=True)
	Dificultad_asig=models.DecimalField(default=0,max_digits=3, decimal_places=1)
	Nivel_resuelto=models.DecimalField(default=1,max_digits=3, decimal_places=1)
	Resuelto = models.BooleanField(default=False)
	Clasificado = models.BooleanField(default=False)
	Recomendado = models.BooleanField(default=False)
	Contabilizado= models.BooleanField(default=False)
	Categoria1 = models.CharField(max_length=45,blank=True)
	Categoria2 = models.CharField(max_length=45,blank=True)
	Categoria3 = models.CharField(max_length=45,blank=True)
	
	def __str__(self):
		return str(self.Usuario_id)+"-"+str(self.Problema_id)


class Pertenece(models.Model):
	Categoria_id = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	Problema_id = models.ForeignKey(Problema, on_delete=models.CASCADE)
	Votos = models.IntegerField(default=0)
	
	
	def __str__(self):
		return str(self.Problema_id)+"-"+str(self.Categoria_id)

class Habilidad(models.Model):
	Categoria_id = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	User_id = models.ForeignKey(Usuario, on_delete=models.CASCADE)
	Resueltos = models.IntegerField(default=0)
	Acumulada_num = models.FloatField(default=0.0)
	Acumulada_dem = models.FloatField(default=0.0)
	Nivel = models.DecimalField(default=0.0,max_digits=3, decimal_places=1)
	def __str__(self):
		return str(self.Categoria_id)+"-"+str(self.User_id)


