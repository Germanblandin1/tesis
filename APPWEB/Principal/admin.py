from django.contrib import admin

from .models import Usuario
from .models import Juez
from .models import Categoria
from .models import Problema
from .models import Tutorial
from .models import Cuenta
from .models import Resuelve
from .models import Pertenece
from .models import Habilidad
from .models import Idioma
from .models import Pais

# Register your models here.

admin.site.register(Usuario)
admin.site.register(Juez)
admin.site.register(Categoria)
admin.site.register(Problema)
admin.site.register(Tutorial)
admin.site.register(Cuenta)
admin.site.register(Resuelve)
admin.site.register(Pertenece)
admin.site.register(Habilidad)
admin.site.register(Idioma)
admin.site.register(Pais)