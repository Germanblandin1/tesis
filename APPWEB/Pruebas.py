import requests
import json
import sys
import time
import subprocess
from Principal.models import *
from decimal import *
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS, ObjectDoesNotExist

def dame_categoria(problem):
	Obj=Pertenece.objects.filter(Problema_id=problem)
	if(Obj):
		Obj=Obj.order_by('-Votos')[0]
		return Obj.Categoria_id
	return None

#falta probar
def sumar_habilidad(User,Categ,Dif):
	if Dif!=None and Dif!=0:
		Hab=Habilidad.objects.get(User_id=User,Categoria_id=Categ)
		User.Nivel_num-=float(Hab.Nivel)
		Hab.Resueltos+=1
		Hab.Acumulada_num+=float(float(Dif)*float(Dif))
		Hab.Acumulada_dem+=float(Dif)
		Hab.Nivel = Hab.Acumulada_num/Hab.Acumulada_dem
		Hab.save()
		User.Nivel_num+=float(Hab.Nivel)
		User.Nivel=Decimal(User.Nivel_num/User.Nivel_dem)
		User.save()

#falta probar
def crear_habilidades(User):
	Habilidad.objects.filter(User_id=User).delete()
	can=0
	for C in Categoria.objects.all():
		Nuevo_Hab=Habilidad(Categoria_id=C,User_id=User,Resueltos=0,Acumulada_num=0,Acumulada_dem=0,Nivel=0)
		Nuevo_Hab.save()
		can+=1
	User.Nivel=1
	User.Nivel_dem=can-1
	User.Nivel_num=0
	User.save()

r = requests.get('http://codeforces.com/api/contest.standings?contestId=754&from=1&count=50&showUnofficial=false')
data = r.json()
Usuarios=[]
if data["status"]=="OK" and r.status_code==200:
	for P in data["result"]["rows"]:
		for U in P["party"]["members"]:
			Usuarios.append(U["handle"])

print("Ya busco los usuarios hay "+str(len(Usuarios)))

Usuario.objects.all().delete()
Cuenta.objects.all().delete()
Resuelve.objects.all().delete()
Habilidad.objects.all().delete()
print("Ya borro lo anterior")
can=0
Judge=Juez.objects.get(Nombre="Codeforces")
print("ya tiene juez")
print("Ya busco los usuarios hay "+str(len(Usuarios)))
for U in Usuarios:
	print("hola")
	nombre="Usuario"+str(can)
	apellido=str(can)
	correo=U+"@host.com"
	print(correo)
	pais="EEUU"
	nickname=U
	password="123456"
	User=Usuario(Nombre=nombre,Apellido=apellido,Correo=correo,Pais=pais,Nickname=nickname,Password=password)
	valido=True
	try:
		User.validate_unique()
		User.save()
		crear_habilidades(User)
	except ValidationError as e:
		valido=False
	if(valido):
		Cuenta_Nueva=Cuenta(Usuario_id=User,Juez_id=Judge,ID_cuenta=U,Problemas_resueltos=0,Problemas_pendientes=0)
		Cuenta_Nueva.save()
	print("VERGA")
	can+=1

print("Termino de poner los usuarios")
for User in Usuario.objects.all():
	con=10000
	print("hola verga")
	cuentas=Cuenta.objects.filter(Usuario_id=User)
	for C in cuentas:
		Crawler_juez=C.Juez_id.Crawler
		J=C.Juez_id
		name_acount=C.ID_cuenta
		query=['scrapy','crawl',Crawler_juez+'_user',"-a","usuario="+name_acount,"-a","con="+str(con)]
		subprocess.run(args=query)
		#time.sleep(5)
		filename="data/"+Crawler_juez+"_"+name_acount+".data"
		with open(filename, "r",encoding="utf-8", errors="surrogateescape") as f:
			linea=f.readline()
			while linea!="":
				Lista=linea.split()
				Nombre=Lista[0]
				dif=Decimal(float(Lista[1]))
				Descripcion=f.readline().split("\n")[0]
				URL=f.readline().split("\n")[0]
				tam=int(f.readline().split("\n")[0])
				try:
					OBJ=Problema.objects.get(Nombre=Nombre,URL=URL,Descripcion=Descripcion,Juez_id=J)
					if(tam>0):
						Lista=f.readline().split()
				except ObjectDoesNotExist:
					OBJ=Problema(Nombre=Nombre,URL=URL,Descripcion=Descripcion,Juez_id=J,Dificultad_estadisticas=dif)
					OBJ.save()
					if(tam>0):
						Lista=f.readline().split()
						for categoria in Lista:
							for C in Categoria.objects.all():
								if(categoria in C.Otros):
									Relacion = Pertenece(Problema_id=OBJ,Categoria_id=C,Votos=20)
									Relacion.save()
									break
				#aqui ya esta seguro que el problema esta guardadoen la BD
				#por tanto guardamos los resuelve
				nivel=1
				if(User.Nivel!=None):
					nivel=User.Nivel
				Categ=dame_categoria(OBJ)
				try:
					AUX=Resuelve.objects.get(Usuario_id=User, Problema_id=OBJ)
					if(AUX.Resuelto!=True and Categ!=None):
						sumar_habilidad(User,Categ,OBJ.Dificultad_estadisticas)
					AUX.Resuelto=True
					AUX.Recomendado=False
					AUX.save()
				except ObjectDoesNotExist:
					AUX=Resuelve(Usuario_id=User, Problema_id=OBJ,Juez_id=J,Resuelto=True,Nivel_resuelto=nivel,Recomendado=False)
					AUX.save()
					if(Categ!=None):
						sumar_habilidad(User,Categ,OBJ.Dificultad_estadisticas)
				linea=f.readline()
			print("terminowhile")
		print("termino archivo")
	print("verga")

print("Termino todo")




